package fr.inria.abstoproactive.javabackendwithjavalists.MapReduceTest;

import java.util.List;

import fr.inria.abstoproactive.javabackendwithjavalists.MapReduce.Pair;

// abs:4:1: 
public interface Client_i extends abs.backend.java.lib.types.ABSInterface {
    // abs:5:2: 
    public  List<Pair<String,Pair<Integer,String>>> compute();
}
