package fr.inria.abstoproactive.javabackendwithjavalists.MapReduceTest;
// [INRIA-OASIS] begin
import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.extensions.gcmdeployment.PAGCMDeployment;
import org.objectweb.proactive.gcmdeployment.GCMApplication;
import org.objectweb.proactive.gcmdeployment.GCMVirtualNode;

import fr.inria.abstoproactive.javabackendwithjavalists.MapReduce.MapReduce_c;
import fr.inria.abstoproactive.javabackendwithjavalists.MapReduce.MapReduce_i;
import fr.inria.abstoproactive.javabackendwithjavalists.MapReduce.Pair;
import fr.inria.abstoproactive.javabackendwithjavalists.MapReduce.numNodes_f;


// [INRIA-OASIS] end
public class Main extends abs.backend.java.lib.runtime.ABSObject implements Serializable {
    // [INRIA-OASIS] begin
    private GCMApplication gcma;
    private transient java.util.logging.Logger logger = null;
    // [INRIA-OASIS] end
    public static void main(java.lang.String[] args) throws Exception {
        // [INRIA-OASIS] begin
        GCMApplication app;
        File desc = new File("resources/GCMA.xml");
        if (!desc.exists()) {desc = new File("absfrontend-to-proactive/usecase/resources/GCMA.xml"); }
        if (!desc.exists()) {desc = new File("GCMA.xml"); }
        app = PAGCMDeployment.loadApplicationDescriptor(desc);
        app.startDeployment();
        app.waitReady();
        // [INRIA-OASIS] end
        // [INRIA-OASIS] begin
        // Added an argument to startup that is the GCMApplication in order to
        // access the nodes when creating a new ProActive object then
        abs.backend.java.lib.runtime.StartUp.startup(args,Main.class, app);
        // [INRIA-OASIS] end
        // [INRIA-OASIS] begin
        Map<String, GCMVirtualNode> nodeList = app.getVirtualNodes();
        for (GCMVirtualNode vNode : nodeList.values()) {
            System.out.println("[INRIA-OASIS] virtual node: " + vNode.getName());
            for (Node node : vNode.getCurrentNodes()) {
                System.out.println("    [INRIA-OASIS] number of active objects: " + node.getNumberOfActiveObjects());
                for (Object obj : node.getActiveObjects()) {
                    System.out.println("        [INRIA-OASIS] active object: " + obj.getClass().getSimpleName());
                }
            }
        }
        if (app != null) {
            app.kill();
        }
        // [INRIA-OASIS] end
    }
    public java.lang.String getClassName() { return "Main"; }
    public java.util.List<java.lang.String> getFieldNames() { return java.util.Collections.EMPTY_LIST; }
    // [INRIA-OASIS] begin
    // Added gcma
    public Main(abs.backend.java.lib.runtime.COG cog, GCMApplication gcma) { super(cog); this.setLocalCOG(cog); this.gcma = gcma; }
    // [INRIA-OASIS] end
    public abs.backend.java.lib.types.ABSUnit run() {
    	long begin = System.currentTimeMillis();
        if (logger == null) { 
        try {
            logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
            logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
            logger.setLevel(java.util.logging.Level.FINEST);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
    logger.finest("[INRIA-OASIS] Executing method: run on target object: " + this + "\n");
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().newStackFrame(this,"main block");
    
    MapReduce_i m = MapReduce_c.__ABS_createNewObject(this);

    
    Client_i c = Client_c.__ABS_createNewObject(this, m);

    
    List<Pair<String,Pair<Integer,String>>> result = abs.backend.java.lib.runtime.ABSRuntime.checkForNull(c).compute();
    for(Pair<String, Pair<Integer, String>> element: result) {
		System.out.println("element.first: " + element.first);
		System.out.println("element.second.first: " + element.second.first);
		System.out.println("element.second.second: " + element.second.second);
	}
	long end = System.currentTimeMillis();
	
	System.out.println("Generated ProActive with Java lists - number of nodes: " + numNodes_f.apply());
	System.out.println("Result computed - total time: " + (end-begin) + "ms.");
    logger.finest(result.toString());
    logger.finest("[INRIA-OASIS] Ending method: run on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}
