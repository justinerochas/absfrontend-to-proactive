#!/usr/bin/env python


import os

import inspect

filename = inspect.getfile(inspect.currentframe())
print 'Running', filename

# Open Client_c file
tmp_client_file = open('Client_c_tmp.java', 'w')
client_file = open('Client_c.java', 'r')

for line in client_file:
	if 'new ABS.StdLib.Pair_Pair' in line:
		tmp_line1 = line.replace('new ABS.StdLib.Pair_Pair(abs.backend.java.lib.types.ABSString.fromString(', 'new Pair<String,String>(')
		tmp_line2 = tmp_line1.replace('), abs.backend.java.lib.types.ABSString.fromString(',', ')
		tmp_line3 = tmp_line2.replace('new',  '\n new')
		tmp_client_file.write(tmp_line3)
	else:
		tmp_client_file.write(line)

tmp_client_file.close()
client_file.close()
