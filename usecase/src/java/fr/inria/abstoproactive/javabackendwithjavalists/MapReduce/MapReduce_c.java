package fr.inria.abstoproactive.javabackendwithjavalists.MapReduce;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// abs:261:1: 
public class MapReduce_c extends abs.backend.java.lib.runtime.ABSObject implements abs.backend.java.lib.types.ABSClass, MapReduce_i {
    private static final java.lang.String[] __fieldNames = new java.lang.String[] { "workers", "nWorkers" };
    public final java.util.List<java.lang.String> getFieldNames() { return java.util.Arrays.asList(__fieldNames); }
    // abs:262:3: 
    private List<Worker_i> workers;
    // abs:263:3: 
    private Integer nWorkers;
    private transient java.util.logging.Logger logger = null;
    public MapReduce_c(abs.backend.java.lib.runtime.COG cog) {
        super(cog);
        getCOG().objectCreated(this);
    }
    protected final void __ABS_init() {
        this.workers = new LinkedList<Worker_i>();this.nWorkers = 0 ;getCOG().objectInitialized(this);
    }
    public java.util.UUID getUUID() { return this.uuid; }
    protected final abs.backend.java.lib.types.ABSValue getFieldValue(java.lang.String __ABS_fieldName) throws java.lang.NoSuchFieldException {
        return super.getFieldValue(__ABS_fieldName);
    }
    public final java.lang.String getClassName() { return "MapReduce"; }
    public static final <T extends MapReduce_c> T createNewCOG(org.objectweb.proactive.gcmdeployment.GCMApplication gcma, String gcmVirtualNodeName) { return (T)MapReduce_c.__ABS_createNewCOG(null, gcma, gcmVirtualNodeName); }
    public static final <T extends MapReduce_c> T __ABS_createNewCOG(abs.backend.java.lib.runtime.ABSObject __ABS_source, org.objectweb.proactive.gcmdeployment.GCMApplication gcma, String gcmVirtualNodeName) {
        final abs.backend.java.lib.runtime.ABSRuntime __ABS_runtime = __ABS_source.getCOG().getRuntime();
        final abs.backend.java.lib.runtime.COG __ABS_cog = __ABS_runtime.createCOG(MapReduce_c.class, gcma, gcmVirtualNodeName);
        try {
            MapReduce_c __ABS_result = new MapReduce_c(__ABS_cog);
            ;
            __ABS_runtime.cogCreated(__ABS_result);
            __ABS_result.__ABS_init();
            __ABS_cog.registerObject(__ABS_result.uuid, __ABS_result);
            return (T)__ABS_result;
        } finally {
        }
    }
    public static final <T extends MapReduce_c> T createNewObject() { return (T)MapReduce_c.__ABS_createNewObject(null); }
    public static final <T extends MapReduce_c> T __ABS_createNewObject(abs.backend.java.lib.runtime.ABSObject __ABS_source) {
        MapReduce_c __ABS_result = new MapReduce_c(__ABS_source.getCOG());
        __ABS_result.__ABS_init();
        __ABS_source.getLocalCOG().registerObject(__ABS_result.uuid, __ABS_result);
        return (T)__ABS_result;
    }
    // abs:270:3: 
    public final Worker_i getWorker() {
        if (logger == null) { 
        try {
            logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
            logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
            logger.setLevel(java.util.logging.Level.FINEST);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
    logger.finest("[INRIA-OASIS] Executing method: getWorker on target object: " + this + "\n");
    if (__ABS_getRuntime().debuggingEnabled()) {
        abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
        __ABS_currentTask.newStackFrame(this, "getWorker");
    }
     {
        
        
        if (this.nWorkers < numNodes_f.apply()) {
             {
                
                
                Worker_i w = Worker_c.__ABS_createNewCOG(this, getCOG().getGCMA(), null, this);

                
                workers.add(w);
                
                nWorkers = nWorkers + 1;
            }
        }
        
        org.objectweb.proactive.api.PAFuture.getFutureValue(((abs.backend.java.lib.runtime.ABSObject) this).getCOG().awaitCondition(((abs.backend.java.lib.runtime.ABSObject)this).getUUID(),"conditionc5d87ef1_76b2_40a8_996f_58b53828a383", new abs.backend.java.lib.types.ABSType[]{}, new Class<?>[]{}));
        
        Worker_i w = workers.remove(0);
        
        if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
        logger.finest("[INRIA-OASIS] Ending method: getWorker on target object: " + this);
        return w;
    }
}
public void conditionc5d87ef1_76b2_40a8_996f_58b53828a383() {
      while((! MapReduce_c.this.workers.isEmpty()) != true) {
                    try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace();}}}

// abs:285:3: 
public final abs.backend.java.lib.types.ABSUnit finished(Worker_i w) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: finished on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "finished");
}
 {
    
    
	 workers.add(w);
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: finished on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:290:3: 
public final List<Pair<String,Pair<Integer,String>>> mapReduce(List<Pair<String,String>> items) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: mapReduce on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "mapReduce");
}
 {
    
    
	 List<Object> fMapResults = new LinkedList<Object>();

    
	 Map<String, List<Pair<Integer, String>>> intermediates = new HashMap<String, List<Pair<Integer, String>>>();

    
	 List<Object> fReduceResults = new LinkedList<Object>();

    
	 List<Pair<String, Pair<Integer, String>>> result = new LinkedList<>();

    
    Integer incMapCounter = 0;

    
    Integer current_nb_pairs_per_map = nb_pairs_per_map_f.apply();

    
    while (!items.isEmpty()) {
        
        
        Integer counter = 0;

        
        String key = "";

        
        List<String> value = new LinkedList<String>();

        
        while (counter < current_nb_pairs_per_map && !items.isEmpty()) {
            
            
        	Pair<String, String> item = items.remove(0);
            
        	key = key + item.first;
            
        	value.add(item.second);
            
			counter = counter + 1;
        }
        
        Worker_i w = getWorker();

        
        Object fMap = ((abs.backend.java.lib.runtime.ABSObject)w).getCOG().execute(((abs.backend.java.lib.runtime.ABSObject)w).getUUID(), "invokeMap", new Object[] {key, value});
        
        fMapResults.add(fMap);
        
    }
    
    while (! fMapResults.isEmpty()) {
        
    	Object fMapResult = fMapResults.remove(0);
        org.objectweb.proactive.api.PAFuture.getFutureValue(fMapResult);
        // [INRIA-OASIS] A "get" is about to be performed -> switch to hard limit to block
        getLocalCOG().switchHardLimit(true);
        List<Pair<String,Pair<Integer,String>>> mapResult = (List<Pair<String,Pair<Integer,String>>>)org.objectweb.proactive.api.PAFuture.getFutureValue(fMapResult);
        getLocalCOG().switchHardLimit(false);

        
        while (!mapResult.isEmpty()) {
            
            
        	Pair<String, Pair<Integer, String>> keyValuePair = mapResult.remove(0);
            
        	List<Pair<Integer, String>> inter = intermediates.get(keyValuePair.first);
        	if (inter == null) {
				inter = new LinkedList<Pair<Integer, String>>();
			}
            
        	inter.add(keyValuePair.second);
			intermediates.put(keyValuePair.first, inter);
        }
    }
    
    List<String> keys = new LinkedList<String>();
	keys.addAll(intermediates.keySet());

    
    while (! keys.isEmpty()) {
        
        
    	String key = keys.remove(0);
        
    	List<Pair<Integer, String>> values = intermediates.get(key);

        
        Worker_i w = getWorker();

        
        Object fReduce = ((abs.backend.java.lib.runtime.ABSObject)w).getCOG().execute(((abs.backend.java.lib.runtime.ABSObject)w).getUUID(), "invokeReduce", new Object[] {key, values});

        
        fReduceResults.add(fReduce);
    }
    
    while (!fReduceResults.isEmpty()) {
        
        
    	Object fReduceResult = fReduceResults.remove(0);
        
        org.objectweb.proactive.api.PAFuture.getFutureValue(fReduceResult);
        
        // [INRIA-OASIS] A "get" is about to be performed -> switch to hard limit to block
        getLocalCOG().switchHardLimit(true);
        List<Pair<String,Pair<Integer,String>>> reduceResult = (List<Pair<String,Pair<Integer,String>>>)org.objectweb.proactive.api.PAFuture.getFutureValue(fReduceResult);
        getLocalCOG().switchHardLimit(false);

        
        String key = reduceResult.get(0).first;

        
        Pair<Integer, String> value = reduceResult.get(0).second;

        
        result.add(new Pair<>(key, value));
    }
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: mapReduce on target object: " + this);
    return result;
}
}

}
