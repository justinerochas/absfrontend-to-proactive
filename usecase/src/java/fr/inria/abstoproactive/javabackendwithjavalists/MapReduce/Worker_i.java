package fr.inria.abstoproactive.javabackendwithjavalists.MapReduce;

import java.util.List;

// abs:18:1: 
public interface Worker_i extends abs.backend.java.lib.types.ABSInterface {
    // abs:20:3: 
    public  List<Pair<String,Pair<Integer,String>>> invokeMap(String key, List<String> value);
    // abs:22:3: 
    public  List<Pair<String,Pair<Integer,String>>> invokeReduce(String key, List<Pair<Integer,String>> value);
}
