package fr.inria.abstoproactive.javabackendwithjavalists.MapReduce;

import java.util.LinkedList;
import java.util.List;

// abs:32:1: 
public class Worker_c extends abs.backend.java.lib.runtime.ABSObject implements abs.backend.java.lib.types.ABSClass, Worker_i {
    private static final java.lang.String[] __fieldNames = new java.lang.String[] { "master", "mapResults", "reduceResults" };
    public final java.util.List<java.lang.String> getFieldNames() { return java.util.Arrays.asList(__fieldNames); }
    private MapReduce_i master;
    // abs:33:3: 
    private List<Pair<String,Pair<Integer,String>>> mapResults;
    // abs:34:3: 
    private List<Pair<String,Pair<Integer,String>>> reduceResults;
    private transient java.util.logging.Logger logger = null;
    public Worker_c(abs.backend.java.lib.runtime.COG cog,MapReduce_i master) {
        super(cog);
        this.master = master;
        getCOG().objectCreated(this);
    }
    protected final void __ABS_init() {
        this.mapResults = new LinkedList<Pair<String,Pair<Integer,String>>>();this.reduceResults = new LinkedList<Pair<String,Pair<Integer,String>>>();getCOG().objectInitialized(this);
    }
    public java.util.UUID getUUID() { return this.uuid; }
    protected final abs.backend.java.lib.types.ABSValue getFieldValue(java.lang.String __ABS_fieldName) throws java.lang.NoSuchFieldException {
        if ("master".equals(__ABS_fieldName)) return master;
        return super.getFieldValue(__ABS_fieldName);
    }
    public final java.lang.String getClassName() { return "Worker"; }
    public static final <T extends Worker_c> T createNewCOG(org.objectweb.proactive.gcmdeployment.GCMApplication gcma, String gcmVirtualNodeName, MapReduce_i master) { return (T)Worker_c.__ABS_createNewCOG(null, gcma, gcmVirtualNodeName, master); }
    public static final <T extends Worker_c> T __ABS_createNewCOG(abs.backend.java.lib.runtime.ABSObject __ABS_source, org.objectweb.proactive.gcmdeployment.GCMApplication gcma, String gcmVirtualNodeName, MapReduce_i master) {
        final abs.backend.java.lib.runtime.ABSRuntime __ABS_runtime = __ABS_source.getCOG().getRuntime();
        final abs.backend.java.lib.runtime.COG __ABS_cog = __ABS_runtime.createCOG(Worker_c.class, gcma, gcmVirtualNodeName);
        try {
            Worker_c __ABS_result = new Worker_c(__ABS_cog,master);
            ;
            __ABS_runtime.cogCreated(__ABS_result);
            __ABS_result.__ABS_init();
            __ABS_cog.registerObject(__ABS_result.uuid, __ABS_result);
            return (T)__ABS_result;
        } finally {
        }
    }
    public static final <T extends Worker_c> T createNewObject(MapReduce_i master) { return (T)Worker_c.__ABS_createNewObject(null, master); }
    public static final <T extends Worker_c> T __ABS_createNewObject(abs.backend.java.lib.runtime.ABSObject __ABS_source, MapReduce_i master) {
        Worker_c __ABS_result = new Worker_c(__ABS_source.getCOG(),master);
        __ABS_result.__ABS_init();
        __ABS_source.getLocalCOG().registerObject(__ABS_result.uuid, __ABS_result);
        return (T)__ABS_result;
    }
    // abs:40:3: 
    public final abs.backend.java.lib.types.ABSUnit map(String key, String value) {
        if (logger == null) { 
        try {
            logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
            logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
            logger.setLevel(java.util.logging.Level.FINEST);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }
    logger.finest("[INRIA-OASIS] Executing method: map on target object: " + this + "\n");
    if (__ABS_getRuntime().debuggingEnabled()) {
        abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
        __ABS_currentTask.newStackFrame(this, "map");
    }
     {
        
        
        String raw_value = value;

        
        Integer comma_index = 0;

        
        abs.backend.java.lib.types.ABSBool comma_found = abs.backend.java.lib.types.ABSBool.FALSE;

        
        while (comma_found.negate().toBoolean()) {
            
            
            String letter = raw_value.substring(comma_index, comma_index + 1);

            
            if (letter.equals(";")) {
                 {
                    
                    
                    comma_found = abs.backend.java.lib.types.ABSBool.TRUE;
                }
            }
            else {
                 {
                    
                    
                    comma_index = comma_index + 1;
                }
            }
        }
        
        String pattern = value.substring(0, comma_index + 1);

        
        String content = value.substring(comma_index + 1, comma_index + 1 + (value.length() - pattern.length()));
        
        org.objectweb.proactive.api.PAFuture.getFutureValue(((abs.backend.java.lib.runtime.ABSObject) this).getCOG().suspend());
        
        Integer current_matching_length = 0;

        
        String current_matching_string = "";

        
        Integer max_matching_length = 0;

        
        String max_matching_string = "";

        
        Integer current_index = 0;

        
        Integer pattern_index = 0;

        
        Integer content_index = 0;

        
        while (pattern_index < pattern.length()) {
            
            
            while (content_index < content.length()) {
                
                
                boolean matching = true;

                
                while (matching && pattern_index + current_index < pattern.length() && content_index + current_index < content.length()) {
                    
                    
                	String pattern_letter = pattern.substring(pattern_index + current_index, pattern_index + current_index + 1);
					String content_letter = content.substring(content_index + current_index, content_index + current_index + 1);
                    
                    if (!pattern_letter.equals(content_letter)) {
                         {
                            
                            
                            matching = false;
                        }
                    }
                    else {
                         {
                            
                            
                        	current_matching_length = current_matching_length + 1;
     						current_matching_string = current_matching_string + content_letter;
                            
                            if (current_matching_length > max_matching_length) {
    							max_matching_length = current_matching_length;
    							max_matching_string = current_matching_string;
    						}
                            
                            current_index = current_index + 1;
                        }
                    }
                }
                
                current_index = 0;
				current_matching_length = 0;
				current_matching_string = "";
				content_index = content_index + 1;
            }
            
            content_index = 0;
			pattern_index = pattern_index + 1;
        }
        
        this.emitMapResult(pattern, new Pair<>(max_matching_length, key + ";" + max_matching_string));
        
        if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
        logger.finest("[INRIA-OASIS] Ending method: map on target object: " + this);
        return abs.backend.java.lib.types.ABSUnit.UNIT;
    }
}

// abs:182:3: 
public final abs.backend.java.lib.types.ABSUnit reduce(String key, List<Pair<Integer,String>> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: reduce on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "reduce");
}
 {
    
    
    List<Pair<Integer,String>> results = value;

    
    int max = 0;
	String match = "";

    
	for (Pair<Integer, String> pair: results) {
		if (pair.first > max) {
			max = pair.first;
			match = pair.second;
		}
	}
    
	this.emitReduceResult(key, new Pair<>(max, match));
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: reduce on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:202:3: 
public final abs.backend.java.lib.types.ABSUnit onMapStart(String key, List<String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onMapStart on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onMapStart");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onMapStart on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:205:3: 
public final abs.backend.java.lib.types.ABSUnit onMapEmit(String key, Pair<Integer,String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onMapEmit on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onMapEmit");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onMapEmit on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:208:3: 
public final abs.backend.java.lib.types.ABSUnit onMapFinish() {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onMapFinish on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onMapFinish");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onMapFinish on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:211:3: 
public final abs.backend.java.lib.types.ABSUnit onReduceStart(String key, List<Pair<Integer,String>> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onReduceStart on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onReduceStart");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onReduceStart on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:214:3: 
public final abs.backend.java.lib.types.ABSUnit onReduceEmit(String key, Pair<Integer,String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onReduceEmit on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onReduceEmit");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onReduceEmit on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:217:3: 
public final abs.backend.java.lib.types.ABSUnit onReduceFinish() {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: onReduceFinish on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "onReduceFinish");
}
 {
    
    
    ;
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: onReduceFinish on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:222:3: 
public final List<Pair<String,Pair<Integer,String>>> invokeMap(String key, List<String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: invokeMap on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "invokeMap");
}
 {
    
    
    Worker_c.this.mapResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
    
    List<String> entries = value;

    
    onMapStart(key, entries);
    
    for (String entry: entries) { 	
		this.map(key, entry);
	}
    
    onMapFinish();
    
    ((abs.backend.java.lib.runtime.ABSObject)Worker_c.this.master).getCOG().execute(((abs.backend.java.lib.runtime.ABSObject)Worker_c.this.master).getUUID(), "finished", new abs.backend.java.lib.types.ABSType[] {this});
    
    List<Pair<String,Pair<Integer,String>>> result = Worker_c.this.mapResults;

    
    Worker_c.this.mapResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: invokeMap on target object: " + this);
    return result;
}
}

// abs:238:3: 
public final List<Pair<String,Pair<Integer,String>>> invokeReduce(String key, List<Pair<Integer,String>> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: invokeReduce on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "invokeReduce");
}
 {
    
    
    Worker_c.this.reduceResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
    
    onReduceStart(key, value);
    
    reduce(key, value);
    
    onReduceFinish();
    
    ((abs.backend.java.lib.runtime.ABSObject)Worker_c.this.master).getCOG().execute(((abs.backend.java.lib.runtime.ABSObject)Worker_c.this.master).getUUID(), "finished", new abs.backend.java.lib.types.ABSType[] {this});
    
    List<Pair<String,Pair<Integer,String>>> result = Worker_c.this.reduceResults;

    
    Worker_c.this.reduceResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: invokeReduce on target object: " + this);
    return result;
}
}

// abs:249:3: 
public final abs.backend.java.lib.types.ABSUnit emitMapResult(String key, Pair<Integer,String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: emitMapResult on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "emitMapResult");
}
 {
    
    
    onMapEmit(key, value);
    
    mapResults.add(new Pair<>(key, value));
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: emitMapResult on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

// abs:253:3: 
public final abs.backend.java.lib.types.ABSUnit emitReduceResult(String key, Pair<Integer,String> value) {
    if (logger == null) { 
    try {
        logger = java.util.logging.Logger.getLogger(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID);
        logger.addHandler(new java.util.logging.FileHandler(abs.backend.java.lib.runtime.ABSRuntime.ABS_LOG_PATH + abs.backend.java.lib.runtime.ABSRuntime.ABS_LOGGER_ID));
        logger.setLevel(java.util.logging.Level.FINEST);
    } catch (java.io.IOException e) {
        e.printStackTrace();
    }
}
logger.finest("[INRIA-OASIS] Executing method: emitReduceResult on target object: " + this + "\n");
if (__ABS_getRuntime().debuggingEnabled()) {
    abs.backend.java.lib.runtime.Task<?> __ABS_currentTask = __ABS_getRuntime().getCurrentTask();
    __ABS_currentTask.newStackFrame(this, "emitReduceResult");
}
 {
    
    
    onReduceEmit(key, value);
    
    reduceResults.add(new Pair<>(key, value));
    
    if (__ABS_getRuntime().debuggingEnabled()) __ABS_getRuntime().getCurrentTask().popStackFrame();
    logger.finest("[INRIA-OASIS] Ending method: emitReduceResult on target object: " + this);
    return abs.backend.java.lib.types.ABSUnit.UNIT;
}
}

}
