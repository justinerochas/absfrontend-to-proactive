package fr.inria.abstoproactive.javabackendwithjavalists.MapReduce;

import java.util.List;

// abs:25:1: 
public interface MapReduce_i extends abs.backend.java.lib.types.ABSInterface {
    // abs:27:3: 
    public  List<Pair<String,Pair<Integer,String>>> mapReduce(List<Pair<String,String>> entries);
    // abs:29:3: 
    public  abs.backend.java.lib.types.ABSUnit finished(Worker_i w);
}
