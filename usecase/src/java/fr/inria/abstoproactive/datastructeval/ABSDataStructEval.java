package fr.inria.abstoproactive.datastructeval;

import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSString;

public class ABSDataStructEval {
	
	public static void addInteger(boolean evaluate) {
		long begin = System.currentTimeMillis();
		ABS.StdLib.List<ABSInteger> list  = new ABS.StdLib.List_Nil<ABSInteger>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(42), list);
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(42), list);
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(42), list);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add integer: " + (end-begin) + " ms");
		}
	}
	
	public static void addLoopParameter(boolean evaluate) {
		long begin = System.currentTimeMillis();
		ABS.StdLib.List<ABSInteger> list  = new ABS.StdLib.List_Nil<ABSInteger>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(i), list);
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(i), list);
			list = new ABS.StdLib.List_Cons<ABSInteger>(ABSInteger.fromInt(i), list);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add loop parameter: " + (end-begin) + " ms");
		}
	}
	
	public static void addEmptyString(boolean evaluate) {
		long begin = System.currentTimeMillis();
		ABS.StdLib.List<ABSString> list  = new ABS.StdLib.List_Nil<ABSString>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.EMPTY, list);
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.EMPTY, list);
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.EMPTY, list);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add empty string: " + (end-begin) + " ms");
		}
	}
	
	public static void addString(boolean evaluate) {
		long begin = System.currentTimeMillis();
		ABS.StdLib.List<ABSString> list  = new ABS.StdLib.List_Nil<ABSString>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.fromString("toto"), list);
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.fromString("titi"), list);
			list = new ABS.StdLib.List_Cons<ABSString>(ABSString.fromString("tata"), list);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add string: " + (end-begin) + " ms");
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Before JVM warmup");
		addInteger(true);
		addLoopParameter(true);
		addEmptyString(true);
		addString(true);
		
		for (int i = 0 ; i < 10 ; i++) {
			addInteger(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addLoopParameter(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addEmptyString(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addString(false);
		}
		
		System.out.println("After JVM warm up");
		addInteger(true);
		addLoopParameter(true);
		addEmptyString(true);
		addString(true);
	}
	
}
