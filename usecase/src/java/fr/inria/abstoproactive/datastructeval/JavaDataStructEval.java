package fr.inria.abstoproactive.datastructeval;

import java.util.ArrayList;
import java.util.List;

public class JavaDataStructEval {

	private static void addInteger(boolean evaluate) {
		long begin = System.currentTimeMillis();
		List<Integer> list  = new ArrayList<Integer>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list.add(42);
			list.add(42);
			list.add(42);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add integer: " + (end-begin) + " ms");
		}
	}

	private static void addLoopParameter(boolean evaluate) {
		long begin = System.currentTimeMillis();
		List<Integer> list  = new ArrayList<Integer>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list.add(i);
			list.add(i);
			list.add(i);
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add loop parameter: " + (end-begin) + " ms");
		}
	}
	
	public static void addEmptyString(boolean evaluate) {
		long begin = System.currentTimeMillis();
		List<String> list  = new ArrayList<String>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list.add("");
			list.add("");
			list.add("");
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add empty string: " + (end-begin) + " ms");
		}
	}
	
	public static void addString(boolean evaluate) {
		long begin = System.currentTimeMillis();
		List<String> list  = new ArrayList<String>();
		for (int i = 0 ; i < 1000000 ; i++) {
			list.add(new String("toto"));
			list.add(new String("titi"));
			list.add(new String("tata"));
		}
		long end = System.currentTimeMillis();
		if (evaluate) {
			System.out.println("  - Duration add string: " + (end-begin) + " ms");
		}
	}

	public static void main(String[] args) {
		System.out.println("Before JVM warmup");
		addInteger(true);
		addLoopParameter(true);
		addEmptyString(true);
		addString(true);
		
		for (int i = 0 ; i < 10 ; i++) {
			addInteger(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addLoopParameter(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addEmptyString(false);
		}
		for (int i = 0 ; i < 10 ; i++) {
			addString(false);
		}
		
		System.out.println("After JVM warm up");
		addInteger(true);
		addLoopParameter(true);
		addEmptyString(true);
		addString(true);
	}
	
}
