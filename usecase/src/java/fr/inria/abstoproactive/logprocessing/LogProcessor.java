package fr.inria.abstoproactive.logprocessing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.*;

import abs.backend.java.lib.runtime.ABSRuntime;

import javax.xml.parsers.*;

public class LogProcessor {

private static final int nbNodes = 4;

	// Set up
	private static final String SET_NEXT_NODE_METHOD = "setNextNode";
	private static final String ELECT_LEADER_METHOD = "electLeader";
	private static final String FIND_LEADER_METHOD = "findLeader";
	private static final String ANNOUNCE_LEADER_METHOD = "announceLeader";
	private static final String RETRIEVE_LEADER_METHOD = "retrieveLeader";
	private static final String REINIT_LEADER_METHOD = "reinitLeader";

	// Computation
	private static final String COMPUTE_MATRIX_VECTOR_METHOD = 
			"computeFibNumber";
	private static final String RECEIVE_COMPUTATION_METHOD = 
			"receiveComputation";
	private static final String RECEIVE_PARTIAL_RESULT_METHOD = 
			"receiveResult";

	// Result
	private static final String GET_RESULT_METHOD = "getResult";
	private static final String RUN_METHOD = "run";

	private static final String ERROR_TAG = "failed";

	public static void main(String[] args) {
		try {
			Map<String, Integer> expectedMethodTagRecords = new HashMap<String, Integer>();
			expectedMethodTagRecords.put(SET_NEXT_NODE_METHOD, nbNodes);
			expectedMethodTagRecords.put(ELECT_LEADER_METHOD, 1);
			expectedMethodTagRecords.put(FIND_LEADER_METHOD, 2 * nbNodes - 1);
			expectedMethodTagRecords.put(ANNOUNCE_LEADER_METHOD, nbNodes);
			expectedMethodTagRecords.put(RETRIEVE_LEADER_METHOD, nbNodes);
			//expectedMethodTagRecords.put(REINIT_LEADER_METHOD, nbNodes);
			expectedMethodTagRecords.put(COMPUTE_MATRIX_VECTOR_METHOD, nbNodes);
			expectedMethodTagRecords.put(RECEIVE_COMPUTATION_METHOD, nbNodes);
			expectedMethodTagRecords.put(RECEIVE_PARTIAL_RESULT_METHOD, nbNodes * (nbNodes + 1) / 2);
			expectedMethodTagRecords.put(GET_RESULT_METHOD, 1);
			expectedMethodTagRecords.put(RUN_METHOD, 1);

			Map<String, Integer> methodTagRecords = new HashMap<String, Integer>();
			methodTagRecords.put(SET_NEXT_NODE_METHOD, 0);
			methodTagRecords.put(ELECT_LEADER_METHOD, 0);
			methodTagRecords.put(FIND_LEADER_METHOD, 0);
			methodTagRecords.put(ANNOUNCE_LEADER_METHOD, 0);
			methodTagRecords.put(RETRIEVE_LEADER_METHOD, 0);
			//methodTagRecords.put(REINIT_LEADER_METHOD, 0);
			methodTagRecords.put(COMPUTE_MATRIX_VECTOR_METHOD, 0);
			methodTagRecords.put(RECEIVE_COMPUTATION_METHOD, 0);
			methodTagRecords.put(RECEIVE_PARTIAL_RESULT_METHOD, 0);
			methodTagRecords.put(GET_RESULT_METHOD, 0);
			methodTagRecords.put(RUN_METHOD, 0);		

			Document document = null;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			copyFile(new File(LogProcessor.class.getClassLoader().getResource("logger.dtd").toURI()), 
					new File(ABSRuntime.ABS_LOG_PATH + "logger.dtd"));
			File[] files = new File(ABSRuntime.ABS_LOG_PATH).listFiles();

			List<File> effectiveLogFiles = new LinkedList<File>(); 
			for (File file : files) {
				if (!file.getName().contains(".")) {
					effectiveLogFiles.add(file);
					System.out.println("Log file to be analyzed: " + file);
				}
			}

			int nbRecords;
			Node methodTag;
			int nbMethodTags;
			String methodName;
			NodeList methodTags;
			List<Long> timestamps = new ArrayList<Long>(2);
			for (File logFile : effectiveLogFiles) {
				document = builder.parse(logFile);
				methodTags = document.getElementsByTagName("method");
				nbMethodTags = methodTags.getLength();
				for (int i = 0 ; i < nbMethodTags ; i++) {
					methodName = methodTags.item(i).getTextContent();
					if (methodTagRecords.containsKey(methodName)) {
						nbRecords = methodTagRecords.get(methodName);
						int newNbRecords = nbRecords + 1;
						methodTagRecords.put(methodName, newNbRecords);
						if (methodName.equals(RUN_METHOD)) {
							methodTag = methodTags.item(i);
							while (methodTag != null && !methodTag.getNodeName().equals("millis")) {
								methodTag = methodTag.getPreviousSibling();
							}
							timestamps.add(Long.parseLong(methodTag.getTextContent()));
						}
					}
				}
			}

			int expectedValue;
			for (Entry<String, Integer> entry : methodTagRecords.entrySet()) {
				System.out.println("Recorded: " + entry.getKey() + " - " + entry.getValue());
				expectedValue = expectedMethodTagRecords.get(entry.getKey());
				// expectedValue * 2 because there is one log when entering 
				// the method and one log when exiting the method
				if (expectedValue * 2 != entry.getValue().intValue()) {
					throw new Exception("Expected value: " + expectedValue * 2 + ""
							+ " for entry: " + entry.getKey() + ", and found: " + entry.getValue());
				}
			}

			System.out.println("Running time: " + (timestamps.get(1) - timestamps.get(0)) + "ms");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void copyFile(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}
}


