package fr.inria.abstoproactive.logprocessing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.*;

import abs.backend.java.lib.runtime.ABSRuntime;

import javax.xml.parsers.*;

public class MapReduceLogProcessor {

private static final int nbNodes = 5;
private static final String resultsPath = "/local/home/jrochas/projects/";

	// Result
	private static final String RUN_METHOD = "run";

	public static void main(String[] args) {
		try {
			Map<String, Integer> expectedMethodTagRecords = new HashMap<String, Integer>();
			expectedMethodTagRecords.put(RUN_METHOD, 1);

			Map<String, Integer> methodTagRecords = new HashMap<String, Integer>();
			methodTagRecords.put(RUN_METHOD, 0);		

			Document document = null;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			copyFile(new File(LogProcessor.class.getClassLoader().getResource("logger.dtd").toURI()), 
					new File(ABSRuntime.ABS_LOG_PATH + "logger.dtd"));
			System.out.println("DTD for log files copied.");
			File[] files = new File(ABSRuntime.ABS_LOG_PATH).listFiles();

			List<File> effectiveLogFiles = new LinkedList<File>(); 
			for (File file : files) {
				if (!file.getName().contains(".")) {
					effectiveLogFiles.add(file);
					System.out.println("Log file to be analyzed: " + file);
				}
			}

			int nbRecords;
			Node methodTag;
			int nbMethodTags;
			String methodName;
			NodeList methodTags;
			List<Long> timestamps = new ArrayList<Long>(2);
			for (File logFile : effectiveLogFiles) {
				document = builder.parse(logFile);
				methodTags = document.getElementsByTagName("method");
				nbMethodTags = methodTags.getLength();
				for (int i = 0 ; i < nbMethodTags ; i++) {
					methodName = methodTags.item(i).getTextContent();
					if (methodTagRecords.containsKey(methodName)) {
						nbRecords = methodTagRecords.get(methodName);
						int newNbRecords = nbRecords + 1;
						methodTagRecords.put(methodName, newNbRecords);
						if (methodName.equals(RUN_METHOD)) {
							methodTag = methodTags.item(i);
							while (methodTag != null && !methodTag.getNodeName().equals("millis")) {
								methodTag = methodTag.getPreviousSibling();
							}
							timestamps.add(Long.parseLong(methodTag.getTextContent()));
						}
					}
				}
			}
			
			System.out.println("Running time: " + (timestamps.get(1) - timestamps.get(0)) + "ms");
			int expectedValue;
			for (Entry<String, Integer> entry : methodTagRecords.entrySet()) {
				System.out.println("Recorded: " + entry.getKey() + " - " + entry.getValue());
				expectedValue = expectedMethodTagRecords.get(entry.getKey());
				// expectedValue * 2 because there is one log when entering 
				// the method and one log when exiting the method
				if (expectedValue * 2 != entry.getValue().intValue()) {
					throw new Exception("Expected value: " + expectedValue * 2 + ""
							+ " for entry: " + entry.getKey() + ", and found: " + entry.getValue());
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void copyFile(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}
}


