package fr.inria.abstoproactive.proactiveimplem.mapreduce;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.objectweb.proactive.ActiveObjectCreationException;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.annotation.multiactivity.Compatible;
import org.objectweb.proactive.annotation.multiactivity.DefineGroups;
import org.objectweb.proactive.annotation.multiactivity.DefineRules;
import org.objectweb.proactive.annotation.multiactivity.DefineThreadConfig;
import org.objectweb.proactive.annotation.multiactivity.Group;
import org.objectweb.proactive.annotation.multiactivity.MemberOf;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.core.node.NodeException;
import org.objectweb.proactive.gcmdeployment.GCMApplication;
import org.objectweb.proactive.gcmdeployment.GCMVirtualNode;
import org.objectweb.proactive.multiactivity.MultiActiveService;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.MapReduce;
import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.Worker;

/*
 * class MapReduce implements MapReduce {
 */
@DefineGroups({
	@Group(name="pull", selfCompatible=true),
	@Group(name="release", selfCompatible=true)
})
@DefineRules({
	@Compatible({"pull", "release"})
})
@DefineThreadConfig(threadPoolSize = 10, hardLimit = false)
public class MapReduceImpl implements MapReduce, RunActive {

	private static final long serialVersionUID = 1L;

	private GCMApplication app;

	public MapReduceImpl() {

	}

	public MapReduceImpl(GCMApplication app) {
		this.app = app;
	}

	@Override
	public void runActivity(Body body) {
		MultiActiveService service = new MultiActiveService(body);
		while (body.isActive()) {
			service.multiActiveServing();
		}	
	}

	/*
	 * Set<Worker> workers = EmptySet;
	 * Int nWorkers = 0;
	 */
	private List<Worker> workers = new LinkedList<Worker>();
	private int nWorkers = 0;
	private SerializableObject waitLock = new SerializableObject();

	/*
	 * Worker getWorker() {
	 */
	public Worker getWorker() {
		/*
		 * if (nWorkers < numNodes()) {
		 */
		if (nWorkers < Constants.numNodes) {
			/*
			 * Worker w = new cog Worker(this);
			 */
			// TODO : insert node spec
			Worker w = null;
			try {
				Set<String> p = app.getVirtualNodes().keySet();
                String key = (String) p.toArray()
                        [(int) (new Random().nextInt(p.size()))];
                GCMVirtualNode virtualNode = app.getVirtualNodes().get(key);
				Node node = virtualNode.getANode();
				System.out.println("Deploying on node: " + node.getNodeInformation().getName() + " with URL: " + node.getNodeInformation().getURL());
				w = PAActiveObject.newActive(WorkerImpl.class, new Object[]{PAActiveObject.getStubOnThis()}, node);
			} 
			catch (ActiveObjectCreationException | NodeException e) {
				e.printStackTrace();
			}
			/*
			 * workers = insertElement(workers, w);
			 */
			workers.add(w);
			nWorkers = nWorkers + 1;
		}
		// await ~(emptySet(workers)); ???
		try {
			synchronized(waitLock){
				while (!(workers.size() > 0)) {
					waitLock.wait();
				}
			}
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		/*
		 * Worker w = take(workers);
		 * workers = remove(workers, w);
		 */
		Worker w = workers.remove(0);
		return w;

	}

	/*
	 * Unit finished(Worker w) {
	 */
	@MemberOf("release")
	public SerializableObject finished(Worker w) {
		workers.add(w);
		synchronized(waitLock) {
			waitLock.notifyAll();
		}
		return new SerializableObject();
	}

	/*
	 * List<Pair<String, Pair<Int, String>>> mapReduce(List<Pair<String, String>> items) {
	 */
	@MemberOf("pull")
	public List<Pair<String, Pair<Integer, String>>> mapReduce(List<StringPair> items) {
		/*
		 * List<Fut<List<Pair<String, Pair<Int, String>>>>> fMapResults = Nil;
		 */
		List<List<Pair<String, Pair<Integer, String>>>> fMapResults = new LinkedList<List<Pair<String, Pair<Integer, String>>>>();
		/*
		 * ABS.StdLib.Map<String, List<Pair<Int, String>>> intermediates = EmptyMap;
		 */
		Map<String, List<Pair<Integer, String>>> intermediates = new HashMap<String, List<Pair<Integer, String>>>();
		/*
		 * List<Fut<List<Pair<String, Pair<Int, String>>>>> fReduceResults = Nil;
		 */
		List<List<Pair<String, Pair<Integer, String>>>> fReduceResults = new LinkedList<List<Pair<String, Pair<Integer, String>>>>();
		/*
		 * List<Pair<String, Pair<Int, String>>> result = Nil;
		 */
		List<Pair<String, Pair<Integer, String>>> result = new LinkedList<>();

		int incMapCounter = 0;
		/*
		 * Int current_nb_pairs_per_map = nb_pairs_per_map();
		 */
		int current_nb_pairs_per_map = Constants.NbPairsPerMap;

		/*
		 * while (~isEmpty(items)) {
		 */
		while (!items.isEmpty()) {
			int counter = 0;
			String key = "";
			/*
			 * List<String> value = Nil;
			 */
			List<String> value = new LinkedList<String>();
			/*
			 * while (counter < current_nb_pairs_per_map && ~isEmpty(items)) {
			 */
			while (counter < current_nb_pairs_per_map && !items.isEmpty()) {
				/*
				 * Pair<String, String> item = head(items);
				 * items = tail(items);
				 */	
				StringPair item = items.remove(0);
				/*
				 * key = key + fst(item);
				 */
				key = key + item.first;
				/*
				 * value = Cons(snd(item), value);
				 */
				value.add(item.second);
				counter = counter + 1;
			}
			Worker w = this.getWorker();
			/*
			 * Fut<List<Pair<String, Pair<Int, String>>>> fMap = w!invokeMap(key, value);
			 */
			List<Pair<String, Pair<Integer, String>>> fMap = w.invokeMap(key, value);
			/*
			 * fMapResults = Cons(fMap, fMapResults);
			 */
			fMapResults.add(fMap);
			/*
			 * if (increasing_map_size()) {
			 */
			if (Constants.increasingMapSize) {
				incMapCounter = incMapCounter + 1;
				/*
				 * if (incMapCounter == numNodes()) {
				 */
				if (incMapCounter == Constants.numNodes) {
					current_nb_pairs_per_map = current_nb_pairs_per_map * 2;
					incMapCounter = 0;
				}
			}
		}
		/*
		 * while (~(fMapResults == Nil)) {
		 */
		while (! fMapResults.isEmpty()) {
			/*
			 * Fut<List<Pair<String, Pair<Int, String>>>> fMapResult = head(fMapResults);
			 * fMapResults = tail(fMapResults);//, fMapResult);
			 */
			List<Pair<String, Pair<Integer, String>>> mapResult = fMapResults.remove(0);
			// await fMapResult?; ???
			// List<Pair<String, Pair<Int, String>>> mapResult = fMapResult.get; ???
			/*
			 * while (~isEmpty(mapResult)) {
			 */
			while (!mapResult.isEmpty()) {
				/*
				 * Pair<String, Pair<Int, String>> keyValuePair = head(mapResult);
				 * mapResult = tail(mapResult);
				 */
				Pair<String, Pair<Integer, String>> keyValuePair = mapResult.remove(0);
				/*
				 * List<Pair<Int, String>> inter = lookupDefault(intermediates, fst(keyValuePair), Nil);
				 */
				List<Pair<Integer, String>> inter = intermediates.get(keyValuePair.first);
				if (inter == null) {
					inter = new LinkedList<Pair<Integer, String>>();
				}
				/*
				 * intermediates = put(intermediates, fst(keyValuePair),
				 * 		Cons(snd(keyValuePair), inter));
				 */
				inter.add(keyValuePair.second);
				intermediates.put(keyValuePair.first, inter);
			}
		}

		/*
		 * Set<String> keys = keys(intermediates);
		 */
		List<String> keys = new LinkedList<String>();
		keys.addAll(intermediates.keySet());
		/*
		 * while(~emptySet(keys)) {
		 */
		while(! keys.isEmpty()) {
			/*
			 * String key = take(keys);
			 * keys = remove(keys, key);
			 */
			String key = keys.remove(0);
			/*
			 * List<Pair<Int, String>> values = lookupUnsafe(intermediates, key);
			 */
			List<Pair<Integer, String>> values = intermediates.get(key);
			Worker w = this.getWorker();
			/*
			 * Fut<List<Pair<String, Pair<Int, String>>>> fReduce = w!invokeReduce(key, values);
			 */
			List<Pair<String, Pair<Integer, String>>> fReduce = w.invokeReduce(key, values);
			/*
			 * fReduceResults = Cons(fReduce, fReduceResults);
			 */
			fReduceResults.add(fReduce);
		}
		/*
		 * while (~(fReduceResults == Nil)) {
		 */
		while (!fReduceResults.isEmpty()) {
			/*
			 * Fut<List<Pair<String, Pair<Int, String>>>> fReduceResult = head(fReduceResults);
			 * fReduceResults = tail(fReduceResults);
			 */
			List<Pair<String, Pair<Integer, String>>> reduceResult = fReduceResults.remove(0);
			//await fReduceResult?; ???
			//List<Pair<String, Pair<Int, String>>> reduceResult = fReduceResult.get; ???
			/*
			 * String key = fst(head(reduceResult));
			 */
			String key = reduceResult.get(0).first;
			/*
			 * Pair<Int, String> value = snd(head(reduceResult));
			 */
			Pair<Integer, String> value = reduceResult.get(0).second;
			/*
			 * result = Cons(Pair(key, value), result);
			 */

			result.add(new Pair<>(key, value));
		}
		return result;
	}
}
