package fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces;

import java.util.List;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.Pair;

/*
 * interface Worker {
 *   // invoked by MapReduce component
 *   List<Pair<String, Pair<Int, String>>> invokeMap(String key, List<String> value);
 *   // invoked by MapReduce component
 *   List<Pair<String, Pair<Int, String>>> invokeReduce(String key, List<Pair<Int, String>> value);
 * }
 */
public interface Worker {

	List<Pair<String, Pair<Integer, String>>> invokeMap (String key, List<String> value);
	
	List<Pair<String, Pair<Integer, String>>> invokeReduce (String key, List<Pair<Integer, String>> value);
}
