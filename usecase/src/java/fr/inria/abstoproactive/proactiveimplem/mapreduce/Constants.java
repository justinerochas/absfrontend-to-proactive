package fr.inria.abstoproactive.proactiveimplem.mapreduce;

public class Constants {
	
	/*
	 * def Int numNodes() =4;
	 * def Int nb_pairs_per_map() = 1;
	 * def Bool increasing_map_size() = False;
	*/
	
	public static int numNodes = 50;
	
	public static final int NbPairsPerMap = 1;
	
	public static final boolean increasingMapSize = false;
	
	public static final String ABS_CONFIG_FILENAME = "ABSConfiguration.properties"; 
	
}
