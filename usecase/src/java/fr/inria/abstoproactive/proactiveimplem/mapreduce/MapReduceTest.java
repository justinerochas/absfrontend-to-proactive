package fr.inria.abstoproactive.proactiveimplem.mapreduce;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.extensions.gcmdeployment.PAGCMDeployment;
import org.objectweb.proactive.gcmdeployment.GCMApplication;
import org.objectweb.proactive.gcmdeployment.GCMVirtualNode;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.Client;
import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.MapReduce;

public class MapReduceTest {
	
	public static void main(String[] args) throws Exception {
		long begin = System.currentTimeMillis();
        GCMApplication app;
        File desc = new File("resources/GCMA.xml");
        if (!desc.exists()) {desc = new File("absfrontend-to-proactive/usecase/resources/GCMA.xml"); }
        if (!desc.exists()) {desc = new File("GCMA.xml"); }
        System.out.println("Loading application descriptor: " + desc);
        app = PAGCMDeployment.loadApplicationDescriptor(desc);
        app.startDeployment();
        app.waitReady();
        
        String gcmDefaultVirtualNodeName = null;
		Properties properties = new Properties();
		// Get default cog machine location
		InputStream input = MapReduceTest.class.getClassLoader().getResourceAsStream(Constants.ABS_CONFIG_FILENAME);
		properties.load(input);
		gcmDefaultVirtualNodeName = properties.getProperty("cog.node.name.default");
		Node node = app.getVirtualNode(gcmDefaultVirtualNodeName).getANode();
        
		MapReduce m = PAActiveObject.newActive(MapReduceImpl.class, new Object[]{app}, node);
		System.out.println("Created MapReduce active object");
		Client c = new ClientImpl(m);
		List<Pair<String, Pair<Integer, String>>> result = c.compute();
		for(Pair<String, Pair<Integer, String>> element: result) {
			System.out.println("element.first: " + element.first);
			System.out.println("element.second.first: " + element.second.first);
			System.out.println("element.second.second: " + element.second.second);
		}
		long end = System.currentTimeMillis();
		System.out.println("Native ProActive  - number of nodes: " + Constants.numNodes);
		System.out.println("Result computed - total time: " + (end-begin) + "ms.");
		
		Map<String, GCMVirtualNode> nodeList = app.getVirtualNodes();
        for (GCMVirtualNode vNode : nodeList.values()) {
            System.out.println("[INRIA-OASIS] virtual node: " + vNode.getName());
            for (Node n : vNode.getCurrentNodes()) {
                System.out.println("    [INRIA-OASIS] number of active objects: " + n.getNumberOfActiveObjects());
                for (Object obj : n.getActiveObjects()) {
                    System.out.println("        [INRIA-OASIS] active object: " + obj.getClass().getSimpleName());
                }
            }
        }
	}

}
