package fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces;


import java.util.List;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.Pair;

public interface Client {
	
	List<Pair<String, Pair<Integer, String>>> compute(); 
}
