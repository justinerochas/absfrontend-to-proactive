#!/usr/bin/env python

import sys
import os






tmp_mr_file = open('ClientImplTmp.java', 'w')
mr_file = open('ClientImpl.java', 'r')

for line in mr_file:
	if 'new Pair<String, String>("' in line:
		newline = line.replace('new Pair<String, String>("', 'new StringPair("')
		tmp_mr_file.write(newline)
	else:
		tmp_mr_file.write(line)

tmp_mr_file.close()
mr_file.close()
