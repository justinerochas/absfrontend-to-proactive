package fr.inria.abstoproactive.proactiveimplem.mapreduce;

import java.util.LinkedList;
import java.util.List;

import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.api.PAFuture;
import org.objectweb.proactive.multiactivity.MultiActiveService;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.MapReduce;
import fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces.Worker;

/*
 * class Worker(MapReduce master) implements Worker {
 */
public class WorkerImpl implements Worker, RunActive {

	private MapReduce master;
	
	public WorkerImpl() {
		
	}

	public WorkerImpl(MapReduce master) {
		this.master = master;
	}
	
	@Override
	public void runActivity(Body body) {
		MultiActiveService service = new MultiActiveService(body);
		while (body.isActive()) {
			service.multiActiveServing();
		}	
	}

	/*
	 * List<Pair<String, Pair<Int, String>>> mapResults = Nil;
	 * List<Pair<String, Pair<Int, String>>> reduceResults = Nil;
	 */
	private List<Pair<String, Pair<Integer, String>>> mapResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
	private List<Pair<String, Pair<Integer, String>>> reduceResults = new LinkedList<Pair<String, Pair<Integer, String>>>();	

	/*
	 * Unit map(String key, String value) {
	 */
	private void map(String key, String value) {

		String raw_value = value;

		int comma_index = 0;
		boolean comma_found = false;
		/*
		 * while (~(comma_found)) {
		 */
		while (!comma_found) {
			/*
			 * String letter = substr(raw_value, comma_index, 1);
			 */
			String letter = raw_value.substring(comma_index, comma_index + 1);
			if (letter.equals(";")) {
				comma_found = true;
			}
			else {
				comma_index = comma_index + 1;
			}
		}

		/*
		 * String pattern = substr(value, 0, comma_index);
		 * String content = substr(value, comma_index + 1, strlen(value) - strlen(pattern) - 1);
		 */
		String pattern = value.substring(0, comma_index + 1);
		String content = value.substring(comma_index + 1, comma_index + 1 + (value.length() - pattern.length()));

		// suspend; ???

		int current_matching_length = 0;
		String current_matching_string = "";
		int max_matching_length = 0;
		String max_matching_string = "";

		int current_index = 0;
		int pattern_index = 0;
		int content_index = 0;

		/*
		 * while (pattern_index < strlen(pattern)) {
		 */
		while (pattern_index < pattern.length()) {

			/*
			 * while (content_index < strlen(content)) {
			 */
			while (content_index < content.length()) {

				boolean matching = true;
				/*
				 * while (matching && pattern_index + current_index < strlen(pattern) && content_index + current_index < strlen(content)) {
				 */
				while (matching && pattern_index + current_index < pattern.length() && content_index + current_index < content.length()) {

					/*
					 * String pattern_letter = substr(pattern, pattern_index + current_index, 1);
					 * String content_letter = substr(content, content_index + current_index, 1);
					 */
					String pattern_letter = pattern.substring(pattern_index + current_index, pattern_index + current_index + 1);
					String content_letter = content.substring(content_index + current_index, content_index + current_index + 1);

					/*
					 * if (~(pattern_letter == content_letter)) {
					 */
					if (!pattern_letter.equals(content_letter)) {
						matching = false;
					}
					else {
						current_matching_length = current_matching_length + 1;
						current_matching_string = current_matching_string + content_letter;
						if (current_matching_length > max_matching_length) {
							max_matching_length = current_matching_length;
							max_matching_string = current_matching_string;
						}
						current_index = current_index + 1;
					}
				}
				current_index = 0;
				current_matching_length = 0;
				current_matching_string = "";
				content_index = content_index + 1;
			}
			content_index = 0;
			pattern_index = pattern_index + 1;
		}

		this.emitMapResult(pattern, new Pair<>(max_matching_length, key + ";" + max_matching_string));
	}

	/*
	 * Unit reduce(String key, List<Pair<Int, String>> value) {
	 */
	private void reduce(String key, List<Pair<Integer, String>> value) {

		List<Pair<Integer, String>> results = value;
		int max = 0;
		String match = "";
		/*
		 * while (~(results == Nil)) {
		 * 	Pair<Int, String> local_max = head(results);
		 * 	if (fst(local_max) > max) { 
		 * 		max = fst(local_max);
		 * 		match = snd(local_max);
		 * 	}
		 * 	results = tail(results);
		 * }
		 */
		for (Pair<Integer, String> pair: results) {
			if (pair.first > max) {
				max = pair.first;
				match = pair.second;
			}
		}
		this.emitReduceResult(key, new Pair<>(max, match));
	}

	void onMapStart(String key, List<String> value) {

	}
	void onMapEmit(String key, Pair<Integer, String> value) {

	}
	void onMapFinish() {

	}
	void onReduceStart(String key, List<Pair<Integer, String>> value) {

	}
	void onReduceEmit(String key, Pair<Integer, String> value) {

	}
	void onReduceFinish() {

	}

	/*
	 * List<Pair<String, Pair<Int, String>>> invokeMap(String key, List<String> value) {
	 */
	public List<Pair<String, Pair<Integer, String>>> invokeMap(String key, List<String> value) {
		/*
		 * mapResults = Nil;
		 */
		mapResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
		List<String> entries = value;
		this.onMapStart(key, entries);
		/* 
		 * while (~(entries == Nil)) {
		 *	String entry = head(entries);
		 *	entries = tail(entries);  	
		 *	this.map(key, entry);
		 * }
		 */
		for (String entry: entries) { 	
			this.map(key, entry);
		}
		this.onMapFinish();
		/*
		 * master!finished(this);
		 */
		SerializableObject o = master.finished((Worker) PAActiveObject.getStubOnThis());
		PAFuture.waitFor(o);
		List<Pair<String, Pair<Integer, String>>> result = mapResults;
		/*
		 * mapResults = Nil;
		 */
		mapResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
		return result;
	}

	/*
	 * List<Pair<String, Pair<Int, String>>> invokeReduce(String key, List<Pair<Int, String>> value) {
	 */
	public List<Pair<String, Pair<Integer, String>>> invokeReduce(String key, List<Pair<Integer, String>> value) {
		/*
		 * reduceResults = Nil;
		 */
		reduceResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
		this.onReduceStart(key, value);
		this.reduce(key, value);
		this.onReduceFinish();
		/*
		 * master!finished(this);
		 */
		master.finished((Worker) PAActiveObject.getStubOnThis());
		List<Pair<String, Pair<Integer, String>>> result = reduceResults;
		/*
		 * reduceResults = Nil;
		 */
		reduceResults = new LinkedList<Pair<String, Pair<Integer, String>>>();
		return result;
	}

	/*
	 * Unit emitMapResult(String key, Pair<Int, String> value) {
	 */
	private void emitMapResult(String key, Pair<Integer, String> value) {
		this.onMapEmit(key, value);
		/*
		 * mapResults = Cons(Pair(key, value), mapResults);
		 */
		mapResults.add(new Pair<>(key, value));
	} 

	/*
	 * Unit emitReduceResult(String key, Pair<Int, String> value) {
	 */
	private void emitReduceResult(String key, Pair<Integer, String> value) {
		this.onReduceEmit(key, value);
		/*
		 * reduceResults = Cons(Pair(key, value), reduceResults);
		 */
		reduceResults.add(new Pair<>(key, value));
	}
}
