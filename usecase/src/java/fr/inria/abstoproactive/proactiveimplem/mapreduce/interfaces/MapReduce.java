package fr.inria.abstoproactive.proactiveimplem.mapreduce.interfaces;

import java.io.Serializable;
import java.util.List;

import fr.inria.abstoproactive.proactiveimplem.mapreduce.Pair;
import fr.inria.abstoproactive.proactiveimplem.mapreduce.SerializableObject;
import fr.inria.abstoproactive.proactiveimplem.mapreduce.StringPair;

/*
 * interface MapReduce {
 * 	// invoked by client
 *	List<Pair<String, Pair<Int, String>>> mapReduce(List<Pair<String, String>> entries);
 *	// invoked by workers
 *	Unit finished(Worker w);
 * }
 */
public interface MapReduce extends Serializable {

	List<Pair<String, Pair<Integer, String>>> mapReduce(List<StringPair> entries);
	
	SerializableObject finished(Worker w);
}
