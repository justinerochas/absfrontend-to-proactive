/** 
 * Copyright (c) 2009-2011, The HATS Consortium. All rights reserved. 
 * This file is licensed under the terms of the Modified BSD License.
 */
package abs.backend.java.lib.runtime;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.objectweb.proactive.Body;
import org.objectweb.proactive.RunActive;
import org.objectweb.proactive.annotation.multiactivity.DefineGroups;
import org.objectweb.proactive.annotation.multiactivity.DefineRules;
import org.objectweb.proactive.annotation.multiactivity.DefineThreadConfig;
import org.objectweb.proactive.annotation.multiactivity.Group;
import org.objectweb.proactive.annotation.multiactivity.MemberOf;
import org.objectweb.proactive.annotation.multiactivity.Compatible;
import org.objectweb.proactive.gcmdeployment.GCMApplication;
import org.objectweb.proactive.multiactivity.MultiActiveService;

import abs.backend.java.lib.types.ABSBool;
import abs.backend.java.lib.types.ABSType;
import abs.backend.java.lib.types.ABSUnit;
import abs.backend.java.lib.types.ABSValue;
import abs.backend.java.observing.COGView;
import abs.backend.java.observing.ObjectCreationObserver;
import abs.backend.java.observing.TaskSchedulerView;
import abs.backend.java.scheduling.TaskScheduler;

// [INRIA-OASIS] begin
// Added Serializable
// Added annotations
@DefineGroups({
    @Group(name="absmetadata", selfCompatible=true, minThreads=50, maxThreads=50),
    @Group(name="scheduling", selfCompatible=true),
    @Group(name="waiting", selfCompatible=true, minThreads=50, maxThreads=50)
})
@DefineRules({
    @Compatible({"absmetadata", "scheduling", "waiting"}),
})
@DefineThreadConfig(hardLimit=false, threadPoolSize=101)
public class COG implements ABSValue, Serializable, RunActive {
    private static final long serialVersionUID = 1L;
    // [INRIA-OASIS] end
    // [INRIA-OASIS] begin
    // Removed final keyword for all attributes
    private TaskScheduler scheduler;
    private Class<?> initialClass;
    private int id;
    private ABSRuntime runtime;
    // Added GCMApplication attribute
    private GCMApplication gcma;
    // Added MultiactiveService (transient because we do not need it in a 
    // remote place, it is needed only where it executes).
    private transient MultiActiveService multiActiveService;

    // Store all objects contained in the COG, identified by their ID (this
    // NEEDS to be transient in order not to copy all objects known by the cog 
    // when an ABSObject is given as a parameter of a remote call).
    private transient Map<UUID, ABSType> cogObjects;
    // [INRIA-OASIS] end

    // [INRIA-OASIS] begin
    public COG() {
    }
    // [INRIA-OASIS] end

    /*public COG(ABSRuntime runtime, Class<?> clazz) {
        initialClass = clazz;
        this.runtime = runtime;
        scheduler = runtime.createTaskScheduler(this);
        id = runtime.freshCOGID();
        // [INRIA-OASIS] begin
        try {
            PAActiveObject.turnActive(this);
            System.out.println("[INRIA-OASIS] new COG created (ProActive active object) - ID: " + this.id + " - created in local JVM");
        } catch (ActiveObjectCreationException e) {
            e.printStackTrace();
        } catch (NodeException e) {
            e.printStackTrace();
        }
        // [INRIA-OASIS] end
    }*/

    // [INRIA-OASIS] begin
    // Added GCMApplication argument
    public COG(ABSRuntime runtime, Class<?> clazz, GCMApplication gcma) {
        // [INRIA-OASIS] end
        initialClass = clazz;
        this.runtime = runtime;
        scheduler = runtime.createTaskScheduler(this);
        id = runtime.freshCOGID();
        // [INRIA-OASIS] begin
        this.gcma = gcma;
        this.cogObjects = new HashMap<UUID, ABSType>();
        /*try {
            Set<String> p = this.gcma.getVirtualNodes().keySet();
            String key = (String) p.toArray()[(int) (new Random().nextInt(p.size()))];
            GCMVirtualNode vNode = this.gcma.getVirtualNodes().get(key);
            PAActiveObject.turnActive(this, vNode.getANode(new Random().nextInt(2)));
            System.out.println("[INRIA-OASIS] New COG created (ProActive active object) with ID: " + this.id + " on virtual node: " + vNode.getName());
        } 
        catch (ActiveObjectCreationException e) {
            e.printStackTrace();
        } 
        catch (NodeException e) {
            e.printStackTrace();
        }*/
        // [INRIA-OASIS] end
    }

    // [INRIA-OASIS] begin
    /**
     * {@inheritDoc}
     */
    @Override
    @MemberOf("absmetadata")
    public void runActivity(Body body) {
        System.out.println("[INRIA-OASIS] MultiActiveService started for cog: " + this);
        this.multiActiveService = new MultiActiveService(body);
        this.multiActiveService.multiActiveServing();
    }

    @MemberOf("absmetadata")
    public Boolean switchHardLimit(boolean hardLimit) {
        this.multiActiveService.getRequestExecutor().switchHardLimit(hardLimit);
        return hardLimit;
    }

    @MemberOf("absmetadata")
    public GCMApplication getGCMA() {
        return this.gcma;
    }
    // [INRIA-OASIS] end

    @MemberOf("absmetadata")
    public ABSRuntime getRuntime() {
        return runtime;
    }

    @MemberOf("absmetadata")
    public Class<?> getInitialClass() {
        return initialClass;
    }

    @MemberOf("absmetadata")
    public TaskScheduler getScheduler() {
        return scheduler;
    }

    // [INRIA-OASIS] begin
    boolean finished = false;
    // [INRIA-OASIS] end
    @MemberOf("absmetadata")
    public int addTask(final Task<?> task) {
        // [INRIA-OASIS] begin
        // Execute the task directly and blocks until finished
        new ABSThread() {
            @Override
            public void run() {
                System.out.println("[INRIA-OASIS] COG with id: " + COG.this.id + 
                        " running task: " + task.getCall().methodName());
                setCOG(COG.this);
                task.run();
                synchronized (task) {
                    COG.this.finished = true;
                    task.notifyAll();
                }
            }
        }.start();
        // We need to wait until the task is finished
        synchronized (task) {
            while (!this.finished) {
                try {
                    task.wait();
                } 
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        // We need to return an int (primitive value) in order to make the 
        // addTask remote call synchronous
        return 0;
        //scheduler.addTask(task);
        // [INRIA-OASIS] end
    }

    @MemberOf("absmetadata")
    public void registerObject(UUID objectID, ABSObject object) {
        object.setLocalCOG(this);
        this.cogObjects.put(objectID, object);
    }

    /*@MemberOf("scheduling")
    public ABSValue execute(UUID objectID, String methodName, ABSType[] args) {
        ABSType target = this.cogObjects.get(objectID);
        if (target != null) {
            System.out.println("[INRIA-OASIS] Reifying call: " + methodName + " - from target: " + target + " - from COG: " + ((ABSObject) target).getCOG());
            try {
                Method[] methods = target.getClass().getDeclaredMethods();
                List<Method> matchingMethods = new LinkedList<Method>();
                for (Method method : methods) {
                    if (method.getName().equals(methodName)) {
                        matchingMethods.add(method);
                    }
                }
                // We need to check the parameters of the call:
                // either they are from the same COG, and in this case we need to retrieve the actual reference,
                // or they are in another COG and in this case, the copy is sufficient since any call on it will 
                // be asynchronous (thus sent to their COG)
                ABSType[] actualArgs = new ABSType[args.length];
                int i = 0;
                for (ABSType arg : args) {
                    ABSType actualArg;
                    if (arg instanceof ABSObject) {
                        UUID argId = ((ABSObject) arg).getUUID();
                        ABSType potentialActualArg = this.cogObjects.get(argId);
                        if (potentialActualArg != null) {
                            System.out.println("[INRIA-OASIS] Parameter of type "
                                    + "" + arg.getClass().getSimpleName() 
                                    + "has been found in the local COG.");
                            actualArg = potentialActualArg;
                        }
                        else {
                            actualArg = arg;
                        }
                    }
                    else {
                        actualArg = arg;
                    }
                    actualArgs[i] = actualArg;
                    i++;
                }

                // Choosing now the right method to call
                if (matchingMethods.size() == 1) {
                    if (matchingMethods.get(0).getParameterTypes().length == args.length) {
                        System.out.println("[INRIA-OASIS] Exact match found");
                        return (ABSValue) matchingMethods.get(0).invoke(target, (Object[]) args);
                    }
                    else {
                        System.out.println("[INRIA-OASIS] ERROR : execute - the only matching method does not have the expected number of parameters");
                    }
                }
                else {
                    boolean matchFound = true;
                    Method methodToInvoke = null;
                    // Mimics the JVM method resolution (exact match first)
                    for (Method method : matchingMethods) {
                        if (method.getParameterTypes().length == args.length) {
                            int j = 0;
                            methodToInvoke = method;
                            for (Class<?> type : method.getParameterTypes()) {
                                if (!type.equals(args[j].getClass())) {
                                    matchFound = false;
                                }
                                j++;
                            }
                            if (matchFound) {
                                System.out.println("[INRIA-OASIS] Exact match found");
                                return (ABSValue) methodToInvoke.invoke(target, (Object[]) args);
                            }
                        }
                    }
                    matchFound = true;
                    // An exact match has not been found 
                    for (Method method : matchingMethods) {
                        if (method.getParameterTypes().length == args.length) {
                            int j = 0;
                            methodToInvoke = method;
                            for (Class<?> type : method.getParameterTypes()) {
                                if (!type.isAssignableFrom(args[j].getClass())) {
                                    matchFound = false;
                                }
                                j++;
                            }
                            if (matchFound) {
                                return (ABSValue) methodToInvoke.invoke(target, (Object[]) args);
                            }
                        }
                    }
                    System.out.println("[INRIA-OASIS] ERROR : execute - there is no matching method");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
        else {
            System.out.println("[INRIA-OASIS] ERROR - execute : Object not found in COG hashmap");  
            return null;
        }
    } */
    
    @MemberOf("scheduling")
    public Object execute(UUID objectID, String methodName, Object[] args) {
        ABSType target = this.cogObjects.get(objectID);
        if (target != null) {
            System.out.println("[INRIA-OASIS] Reifying call: " + methodName + " - from target: " + target + " - from COG: " + ((ABSObject) target).getCOG());
            /*Class<?>[] parameterTypes = new Class[args.length];
            /int i = 0;
            for (ABSType obj : args) {
                System.out.println("calling find super ineterf");
                Class<?> interf = findSuperInterface(obj.getClass(), false);
                parameterTypes[i++] = interf;
            }*/
            //Method method;
            try {
                /* method = target.getClass().getDeclaredMethod(methodName, parameterTypes);
                return (ABSType) method.invoke(target, (Object[]) args);*/

                Method[] methods = target.getClass().getDeclaredMethods();
                List<Method> matchingMethods = new LinkedList<Method>();
                for (Method method : methods) {
                    if (method.getName().equals(methodName)) {
                        matchingMethods.add(method);
                    }
                }

                // We need to check the parameters of the call:
                // either they are from the same COG, and in this case we need to retrieve the actual reference,
                // or they are in another COG and in this case, the copy is sufficient since any call on it will be asynchronous (thus send to their COG)
                Object[] actualArgs = new Object[args.length];
                int i = 0;
                for (Object arg : args) {
                    Object actualArg;
                    if (arg instanceof ABSObject) {
                        UUID argId = ((ABSObject) arg).getUUID();
                        ABSType potentialActualArg = this.cogObjects.get(argId);
                        if (potentialActualArg != null) {
                            System.out.println("[INRIA-OASIS] Parameter of type "
                                    + "" + arg.getClass().getSimpleName() 
                                    + "has been found in the local COG.");
                            actualArg = potentialActualArg;
                        }
                        else {
                            actualArg = arg;
                        }
                    }
                    else {
                        actualArg = arg;
                    }
                    actualArgs[i] = actualArg;
                    i++;
                }

                // Choosing now the right method to call
                if (matchingMethods.size() == 1) {
                    if (matchingMethods.get(0).getParameterTypes().length == args.length) {
                        System.out.println("[INRIA-OASIS] Exact match found");
                        return matchingMethods.get(0).invoke(target, (Object[]) args);
                    }
                    else {
                        System.out.println("[INRIA-OASIS] ERROR : execute - the only matching method does not have the expected number of parameters");
                    }
                }
                else {
                    boolean matchFound = true;
                    Method methodToInvoke = null;
                    // Mimics the JVM method resolution (exact match first)
                    for (Method method : matchingMethods) {
                        if (method.getParameterTypes().length == args.length) {
                            int j = 0;
                            methodToInvoke = method;
                            for (Class<?> type : method.getParameterTypes()) {
                                if (!type.equals(args[j].getClass())) {
                                    matchFound = false;
                                }
                                j++;
                            }
                            if (matchFound) {
                                System.out.println("[INRIA-OASIS] Exact match found");
                                return methodToInvoke.invoke(target, (Object[]) args);
                            }
                        }
                    }
                    matchFound = true;
                    // An exact match has not been found 
                    for (Method method : matchingMethods) {
                        if (method.getParameterTypes().length == args.length) {
                            int j = 0;
                            methodToInvoke = method;
                            for (Class<?> type : method.getParameterTypes()) {
                                if (!type.isAssignableFrom(args[j].getClass())) {
                                    matchFound = false;
                                }
                                j++;
                            }
                            if (matchFound) {
                                return methodToInvoke.invoke(target, (Object[]) args);
                            }
                        }
                    }
                    System.out.println("[INRIA-OASIS] ERROR : execute - there is no matching method");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
        else {
            System.out.println("[INRIA-OASIS] ERROR - execute : Object not found in COG hashmap");  
            return null;
        }
    }

    @MemberOf("absmetadata")
    public ABSType getObjectByKey(UUID objectID) {
        return this.cogObjects.get(objectID);
    }

    @MemberOf("waiting")
    public ABSType awaitCondition(UUID objectID, String methodName, ABSType[] args, Class<?>[] argsClass) {
        ABSType target = this.cogObjects.get(objectID);
        if (target != null) {
            System.out.println("[INRIA-OASIS] Waiting on condition: " + methodName + " from target: " + target.getClass().getSimpleName());
            Method method;
            try {
                method = target.getClass().getDeclaredMethod(methodName, argsClass);
                method.invoke(target, (Object[]) args);
                System.out.println("[INRIA-OASIS] Finished awaiting condition: " + methodName + " from target: " + target.getClass().getSimpleName());
                return ABSUnit.UNIT;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
        else {
            System.out.println("[INRIA-OASIS] ERROR - awaitCondition : Object not found in COG hashmap");
            return null;
        }
    }
    
    @MemberOf("waiting")
    public ABSType suspend() {
        return ABSUnit.UNIT;
    }

    @MemberOf("absmetadata")
    public int getID() {
        return id;
    }

    @MemberOf("absmetadata")
    public String toString() {
        return "COG [" + initialClass.getSimpleName() + "] (" + getID() + ")" + super.toString();
    }

    @MemberOf("absmetadata")
    public void objectCreated(ABSObject absObject) {
        if (view != null)
            view.objectCreated(absObject);
    }

    @MemberOf("absmetadata")
    public void objectInitialized(ABSObject absObject) {
        if (view != null)
            view.objectInitialized(absObject);
    }

    @MemberOf("absmetadata")
    public synchronized COGView getView() {
        if (view == null) {
            view = new View();
        }
        return view;
    }

    @MemberOf("absmetadata")
    public void register(ABSObject absObject) {
        // nothing to do
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool eq(ABSValue o) {
        return ABSBool.fromBoolean(this.equals(o));
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool notEq(ABSValue o) {
        return eq(o).negate();
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool gt(ABSValue other) {
        if (other.getClass() == COG.class) {
            return ABSBool.fromBoolean(this.id > ((COG)other).getID());
        } else {
            // type error, not reached
            return ABSBool.FALSE;
        }
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool lt(ABSValue other) {
        if (other.getClass() == COG.class) {
            return ABSBool.fromBoolean(this.id < ((COG)other).getID());
        } else {
            // type error, not reached
            return ABSBool.FALSE;
        }
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool gtEq(ABSValue other) {
        if (other.getClass() == COG.class) {
            return eq(other).or(gt(other));
        } else {
            // type error, not reached
            return ABSBool.FALSE;
        }
    }

    @Override
    @MemberOf("absmetadata")
    public ABSBool ltEq(ABSValue other) {
        if (other.getClass() == COG.class) {
            return eq(other).or(lt(other));
        } else {
            // type error, not reached
            return ABSBool.FALSE;
        }
    }

    @Override
    @MemberOf("absmetadata")
    public boolean isDataType() {
        return false;
    }

    @Override
    @MemberOf("absmetadata")
    public boolean isReference() {
        return true;
    }

    private View view;

    private class View implements COGView {
        private List<ObjectCreationObserver> creationListeners;
        private Map<String, List<ObjectCreationObserver>> creationClassListeners;

        synchronized void notifyListeners(ABSObject absObject, boolean created) {
            if (creationListeners != null) {
                for (ObjectCreationObserver l : creationListeners) {
                    if (created)
                        l.objectCreated(absObject.getView());
                    else
                        l.objectInitialized(absObject.getView());
                }
            }

            if (creationClassListeners != null) {
                List<ObjectCreationObserver> list = creationClassListeners.get(absObject.getClassName());
                if (list != null) {
                    for (ObjectCreationObserver l : list) {
                        if (created)
                            l.objectCreated(absObject.getView());
                        else
                            l.objectInitialized(absObject.getView());
                    }
                }
            }

        }

        synchronized void objectCreated(ABSObject absObject) {
            notifyListeners(absObject, true);
        }

        synchronized void objectInitialized(ABSObject absObject) {
            notifyListeners(absObject, false);
        }

        @Override
        public synchronized void registerObjectCreationListener(ObjectCreationObserver listener) {
            if (creationListeners == null) {
                creationListeners = new ArrayList<ObjectCreationObserver>(1);
            }
            creationListeners.add(listener);
        }

        @Override
        public synchronized void registerObjectCreationListener(String className, ObjectCreationObserver e) {
            if (creationClassListeners == null) {
                creationClassListeners = new HashMap<String, List<ObjectCreationObserver>>();
            }

            List<ObjectCreationObserver> list = creationClassListeners.get(className);
            if (list == null) {
                list = new ArrayList<ObjectCreationObserver>(1);
                creationClassListeners.put(className, list);
            }
            list.add(e);
        }

        @Override
        public TaskSchedulerView getScheduler() {
            return scheduler.getView();
        }

        @Override
        public int getID() {
            return id;
        }

    }

}
