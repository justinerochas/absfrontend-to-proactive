/**
 * Copyright (c) 2009-2011, The HATS Consortium. All rights reserved. 
 * This file is licensed under the terms of the Modified BSD License.
 */
package abs.backend.java.lib.runtime;

import java.io.Serializable;
import java.util.List;

import abs.backend.java.lib.types.ABSRef;
import abs.backend.java.lib.types.ABSType;
import abs.backend.java.lib.types.ABSValue;

// [INRIA-OASIS] begin
// Added Serializable
public interface AsyncCall<T extends ABSRef> extends Serializable {
// [INRIA-OASIS] end 

    // [INRIA-OASIS] begin
    // Changed return type from Object to ABSSerializableObject
    Serializable execute();
    // [INRIA-OASIS] end
    
    String methodName();
    
    List<ABSValue> getArgs();
    
    T getTarget();
    
    ABSObject getSource();

    Task<?> getSender();
    
}