/** 
 * Copyright (c) 2009-2011, The HATS Consortium. All rights reserved. 
 * This file is licensed under the terms of the Modified BSD License.
 */
package abs.backend.java.lib.expr;


import abs.backend.java.lib.types.ABSValue;

public class PatternValue extends Pattern {

    private final ABSValue value;

    public PatternValue(ABSValue value) {
        this.value = value;
    }

    @Override
    public boolean match(ABSValue dt, PatternBinding binding) {
        // [INRIA-OASIS] begin
        // Made particular case when the checked values are ProActive stubs
        if (value.getClass().getSimpleName().startsWith("_Stub") &&
                dt.getClass().getSimpleName().startsWith("_Stub")) {
            return value.hashCode() == dt.hashCode();
        }
        // [INRIA-OASIS] end
        return value.eq(dt).toBoolean();
    }

}
