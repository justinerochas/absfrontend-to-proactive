/**
 * Copyright (c) 2009-2011, The HATS Consortium. All rights reserved. 
 * This file is licensed under the terms of the Modified BSD License.
 */
package abs.backend.java.scheduling;

import java.io.Serializable;
import java.util.List;

import abs.backend.java.scheduling.SimpleTaskScheduler.TaskInfo;


// [INRIA-OASIS] begin
// Added Serializable
public interface ScheduableTasksFilter extends Serializable {
// [INRIA-OASIS] end
    
    List<TaskInfo> filter(List<TaskInfo> scheduableTasks);
    
}
