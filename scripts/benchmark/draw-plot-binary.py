#!/usr/bin/env python

import sys
import os
import matplotlib 
import matplotlib.pyplot as pyplot

unit = 1000

matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)
matplotlib.rcParams.update({'font.size': 20})

fig, ax1 = pyplot.subplots()

p1, = ax1.plot([2, 4, 6, 10, 14, 18, 24, 30, 36, 42, 50], [9186/60, 5140/60, 5125/60, 5329/60, 5672/60, 5655/60, 5528/60, 5963/60, 5923/60, 6013/60, 5987/60], linestyle='--', marker='o', color='r', linewidth=3.5, markersize=9)
p2, = ax1.plot([2, 4, 6, 10, 14, 18, 24, 30, 36, 42, 50], [9844/60, 4924/60, 3371/60, 2116/60, 1523/60, 1187/60, 955/60, 793/60, 629/60, 602/60, 470/60], linestyle='--', marker='s', color='b', linewidth=3.5, markersize=9)
ax1.axis([2, 50, 2, 11000/60])
ax1.set_xlabel('Number of workers')
ax1.set_ylabel('Computing time (min)')

ax2 = ax1.twinx()
p3, = ax2.plot([2, 4, 6, 10, 14, 18, 24, 30, 36, 42, 50], [0.9331906914, 1.0439629848, 1.5201003813, 2.5177447903, 3.7243811554, 4.7647574296, 5.7829091407, 7.5145382309, 9.4133910773, 9.9877242553, 12.7240281211], linestyle='-', marker='D', color='g', linewidth=6, markersize=13)
ax2.axis([2, 50, 0, 15])
ax2.set_ylabel('Speedup (Java/ProActive)') #, color='g')
#for tick in ax2.get_yticklabels():
#    tick.set_color('g')

legend1 = pyplot.legend([p1, p2], ['ABS-Java backend', 'ABS-ProActive backend'], loc=2,prop={'size':18})
pyplot.legend([p3], ['Speedup'], loc=1,prop={'size':18})
pyplot.gca().add_artist(legend1)

pyplot.show()

###########################################
# For displaying the two of them together #
###########################################

#fig, ax1 = pyplot.subplots(2, sharex=True)

#p1, = ax1[0].plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [9186, 5140, 5125, 5329, 5672, 5655, 5528, 5963, 5923, 6013, 5987], linestyle='-', marker='o', color='r', linewidth=1.5, markersize=7)
#p2, = ax1[0].plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [9844, 4924, 3371, 2116, 1523, 1187, 955, 793, 629, 602, 470], linestyle='-', marker='s', color='b', linewidth=1.5, markersize=7)
#ax1[0].axis([1, 25, 2, 11000])
#ax1[0].set_xlabel('Number of nodes (distributed) = number of workers / 2')
#ax1[0].set_ylabel('Computing time (s)')

#ax2 = ax1[0].twinx()
#p3, = ax2.plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [0.9331906914, 1.0439629848, 1.5201003813, 2.5177447903, 3.7243811554, 4.7647574296, 5.7829091407, 7.5145382309, 9.4133910773, 9.9877242553, 12.7240281211], linestyle='--', marker='D', color='g', linewidth=1.5)
#ax2.axis([1, 25, 0, 15])
#ax2.set_ylabel('ProActive backend speedup') #, color='g')
#for tick in ax2.get_yticklabels():
#    tick.set_color('g')

#legend1 = pyplot.legend([p1, p2], ['Java backend (local)', 'ProActive backend (distributed)'], loc=2,prop={'size':16})
#pyplot.legend([p3], ['Speedup'], loc=1,prop={'size':16})
#pyplot.gca().add_artist(legend1)


#p1, = ax1[1].plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [364120.8/unit, 188193.4/unit, 131556.4/unit, 85341.6/unit, 67045.4/unit, 57905.8/unit, 50420.8/unit, 51683.2/unit, 44801.4/unit, 58383/unit, 75789.6/unit], linestyle='-', marker='H', color='m', linewidth=2, markersize=9)
#p2, = ax1[1].plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [322654.6/unit, 173963.8/unit, 121947.6/unit, 81866.8/unit, 65813.6/unit, 57362.2/unit, 50539/unit, 48441.6/unit, 37766.6/unit, 50533.6/unit, 52794.8/unit], linestyle='-', marker='v', color='c', linewidth=2, markersize=9)
#ax1[1].axis([1, 25, 30, 370])
#ax1[1].set_xlabel('Number of nodes (distributed) = number of workers / 2')
#ax1[1].set_ylabel('Computing time (s)')

#ax2 = ax1[1].twinx()
#p3, = ax2.plot([1, 2, 3, 5, 7, 9, 12, 15, 18, 21, 25], [(1.1285157565-1)*100, (1.0817963277-1)*100, (1.0787944986-1)*100, (1.0424445563-1)*100, (1.0187164963-1)*100, (1.009476624-1)*100, (0.9976612121-1)*100, (1.0669176906-1)*100, (1.1862704082-1)*100, (1.1553303149-1)*100, (1.4355504709-1)*100], linestyle='--', marker='d', color='k', linewidth=1.5)
#ax2.axis([1, 25, 0, 100])
#ax2.set_ylabel('ProActive backend overhead (%)')
#for tick in ax2.get_yticklabels():
#    tick.set_color('y')

#legend1 = pyplot.legend([p1, p2], ['Native ProActive', 'ProActive backend with \nJava data structures'], loc=2)
#pyplot.legend([p3], ['Overhead'], loc=1)
#pyplot.gca().add_artist(legend1)


#pyplot.show()







