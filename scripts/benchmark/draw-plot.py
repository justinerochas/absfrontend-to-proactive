#!/usr/bin/env python

import sys
import os 
import matplotlib.pyplot as pyplot

fig, ax1 = pyplot.subplots()

p1, = ax1.plot([2, 5, 10, 15, 20, 25, 30, 35, 40], [9487, 5255, 4975, 4929, 4661, 5074, 5837, 6625, 6656], linestyle='-', marker='o', color='r')
p2, = ax1.plot([2, 5, 10, 15, 20, 25, 30, 35, 40], [9841, 3875, 1988, 1393, 1053, 907, 776, 634, 595], linestyle='-', marker='D', color='b')
ax1.axis([2, 40, 0, 10000])
ax1.set_xlabel('Number of nodes/workers')
ax1.set_ylabel('Computing time (s)')

ax2 = ax1.twinx()
p3, = ax2.plot([2, 5, 10, 15, 20, 25, 30, 35, 40], [9487/float(9841), 5255/float(3875), 4975/float(1988), 4929/float(1393), 4661/float(1053), 5074/float(907), 5837/float(776), 6625/float(634), 6656/float(595)], linestyle='--', marker='s', color='g')
ax2.axis([2, 40, 0, 12])
ax2.set_ylabel('Speedup', color='g')
for tick in ax2.get_yticklabels():
    tick.set_color('g')

legend1 = pyplot.legend([p1, p2], ['Local', 'Distributed'], loc=2)
pyplot.legend([p3], ['Speedup'], loc=1)
pyplot.gca().add_artist(legend1)
pyplot.show()








