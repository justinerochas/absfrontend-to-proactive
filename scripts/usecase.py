#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


working_dir = os.getcwd()

os.chdir(config.main_site + '/' + config.script_path)

execfile(config.core_deploy_script)

os.chdir(working_dir)


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Ending', filename
