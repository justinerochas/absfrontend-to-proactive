
#!/usr/bin/env python

import os
import config
import inspect
import subprocess


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


working_dir = os.getcwd()

os.chdir(config.main_site + '/' + config.script_path)

execfile('upload/fill_path.py')

execfile(config.prepare_main_script)

os.chdir(working_dir)

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Ending', filename
