#!/usr/bin/env python

import os
import config
import inspect

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename

working_dir = os.getcwd()

os.chdir("../usecase")
os.system("../apache-ant-1.9.3/bin/ant abs.test.java")

os.chdir(working_dir)

print config.logger_name, 'Ending', filename

