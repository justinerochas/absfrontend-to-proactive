#!/usr/bin/env python

import os
import sys
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Retrieve log path (in the ABS configuration file)
abs_config_file = open(config.abs_config_file_name, 'r')
log_file_path = ''
for line in abs_config_file:
	if config.log_path_property in line:
		log_file_path = line.split(' ')[-1]

abs_config_file.close()


# Remove previous results or create result directory
if os.path.exists(log_file_path):
	for afile in os.listdir(log_file_path):
		os.remove(os.path.join(log_file_path, afile))
else:
	os.makedirs(log_file_path)

        
print config.logger_name, 'Ending', filename
