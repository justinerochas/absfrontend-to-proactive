#!/usr/bin/env python

import os
import sys
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Retrieve names of hosts
command = 'scp ' + config.login + '@' + config.access_site + ':' + config.main_site + '/' + config.script_path + '/' + config.host_file_name + ' ./prepare/'
os.system(command)


# Retrieve log file path
abs_config_file = open(config.abs_config_file_name, 'r')
log_file_path = ''
for line in abs_config_file:
	if 'log.path.name' in line:
		log_file_path = line.split(' ')[-1]
abs_config_file.close()
log_directory = log_file_path.split('/')[-1]


# Create a log directory on access site
dir_list = os.popen('ssh ' + config.login + '@' + config.access_site + ' \'ls ' + os.path.dirname(log_file_path) + '\'')
dir_exists = False
for item in dir_list:
	if log_directory in item:
		dir_exists = True
		
if dir_exists:
	command = 'ssh ' + config.login + '@' + config.access_site + ' \'rm ' + log_file_path + '/*\''
	os.system(command)
else:
	command = 'ssh ' + config.login + '@' + config.access_site + ' \'mkdir ' + log_file_path + '\''
	os.system(command)


# Copy logs into log directory of access site
host_file = open(config.host_file_name, 'r')
for line in host_file:
	host = line.split(os.linesep)[0]
	print config.logger_name, "Gathering logs on host: ", host
	command = 'ssh ' + config.login + '@' + config.access_site + ' \'scp ' + config.login + "@" + host + ':' + log_file_path + '/* ' + log_file_path + '\''
	os.system(command)
host_file.close()
	
	
# Copy logs from access site to local
command = 'scp ' + config.login + '@' + config.access_site + ':' + log_file_path + '/* ' + log_file_path
os.system(command) 


# Suppress log from access site
command = 'ssh ' + config.login + '@' + config.access_site + '  \' rm ' + log_file_path + '/*\''
os.system(command)
