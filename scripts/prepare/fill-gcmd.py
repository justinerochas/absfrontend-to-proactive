#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Retrieving hostnames in the file
host_file = open(config.host_file_name, 'r')
hostnames = ''

for line in host_file:
	if hostnames == '':
		hostnames = line[:-1]
	else : 
		hostnames = hostnames + " " + line[:-1]
		
host_file.close()

# Multiply hostnames by two (without first)
hostnames_array = hostnames.split(" ")
del hostnames_array[1]
for i in range(nb_more_deploy_per_machine):
	for hostname in hostnames_array:
		if not first:
			hostnames = hostnames + " " + hostname
		else:
			first = False

# Writing hostnames in GCMD	
tmp_gcmd_file = open(config.tmp_gcmd_file_name, 'w')
gcmd_file = open(config.gcmd_file_name, 'r')
	
for line in gcmd_file:
	if 'sshGroup' in line:
		tmp_gcmd_file.write('<sshGroup id="sshLan" hostList="' + hostnames + '" />' + os.linesep)
	else:
		tmp_gcmd_file.write(line)
		
tmp_gcmd_file.close()
gcmd_file.close()

os.system("mv " + config.tmp_gcmd_file_name + " " + config.gcmd_file_name)


print config.logger_name, 'Ending', filename
