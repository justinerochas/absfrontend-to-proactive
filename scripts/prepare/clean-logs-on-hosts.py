#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


host_file = open(config.host_file_name, 'r')

# Retrieve log path (in the ABS configuration file)
abs_config_file = open(config.abs_config_file_name, 'r')

log_file_path = ''
for line in abs_config_file:
	if config.log_path_property in line:
		log_file_path = line.split(' ')[-1]

abs_config_file.close()
log_directory = log_file_path.split('/')[-1]


for line in host_file:
	host = line.split(os.linesep)[0]
	
	# Checking if the log directory exists
	dir_list = os.popen("ssh " + host + " 'ls " + os.path.dirname(log_file_path) + "'").readlines()
	dir_exists = False
	for item in dir_list:
		if log_directory in item:
			dir_exists = True
	
	# Clearing or creating log directory
	if dir_exists:
		os.system("ssh " + host + " 'rm " + log_file_path + "/*'")
		print config.logger_name, "Clearing logs on host:", host, ". Done."
	else:
		os.system("ssh " + host + " 'mkdir " + log_file_path + "'")
		print config.logger_name, "Creating log directory on host:", host, ". Done."

host_file.close()


print config.logger_name, 'Ending', filename

