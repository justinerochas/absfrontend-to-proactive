#!/usr/bin/env python

import os
import sys
import config
import inspect
import datetime


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename

# Getting current time to reserve nodes on Grid5000
now = datetime.datetime.now()
year = str(now.year)
month = str(now.month) if len(str(now.month)) == 2 else ('0' + str(now.month))
day = str(now.day) if len(str(now.day)) == 2 else ('0' + str(now.day))
hour = str(now.hour) if len(str(now.hour)) == 2 else ('0' + str(now.hour))
minute = str(now.minute) if len(str(now.minute)) == 2 else ('0' + str(now.minute))
second = str(now.second) if len(str(now.second)) == 2 else ('0' + str(now.second))

os.system("ssh " + config.main_site + " \"oargridsub -v -t allow_classic_ssh -s '" + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + "' -w " + sys.argv[1] + " " + sys.argv[2] +":rdef='nodes=" + sys.argv[3] + "'\"")


print config.logger_name, 'Ending', filename
