#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


reservation_line = os.popen("ssh " + config.main_site + " 'oarstat -u " + config.login + " | grep " + config.login + "'").readlines()

# We take the latest reservation only
reservation_id = reservation_line[-1].split()[0]
reserved_nodes = os.popen("ssh " + config.main_site + " 'oarstat -fj " + reservation_id + " | grep assigned_hostnames'").readlines()
hostnames_list = reserved_nodes[0].split()[-1].split('+')

host_file = open(config.host_file_name, 'w')
for hostname in hostnames_list:
	host_file.write(hostname + os.linesep)
	
host_file.close()

# Quick debug print
host_file = open(config.host_file_name, 'r')
for line in host_file:
	print config.logger_name, line[:-1]
	
host_file.close()


print config.logger_name, 'Ending', filename
