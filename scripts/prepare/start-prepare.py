#!/usr/bin/env python

import time
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


execfile('prepare/' + 'reserve-nodes.py')
time.sleep(60)
execfile('prepare/' + 'list-nodes.py')
execfile('prepare/' + 'fill-gcmd.py')
execfile('prepare/' + 'clean-logs-on-hosts.py')


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Ending', filename
