#!/usr/bin/env python

import os

# Program parameters
logger_name = '[ABS-deployment]'
log_path_property = 'log.path.name'
log_directory = 'abs_logs'
abs_config_file_name = '../usecase/resources/ABSConfiguration.properties'

# Grid5000
login = 'jrochas'
access_site = 'access.grid5000.fr'
main_site = 'toulouse'
sites=['grenoble', 'lille', 'lyon', 'nancy', 'reims', 'rennes', 'toulouse', 'sophia']

# Compilation parameters
no_compile_option = '-nocompile'
compile_proactive_option = '-compileproactive'
compile_frontend_option = '-compilefrontend'
compile_all_option = '-compileall'
compilation_options = [no_compile_option, compile_proactive_option, compile_frontend_option, compile_all_option]


# Workflow parameter
compile_proactive_script = 'core/compile-proactive.py'
compile_frontend_script = 'core/compile-frontend.py'

upload_fill_site_script = 'upload/fill_site.py'
upload_main_script = 'upload/upload-absfrontend-on-g5k.py'
upload_usecase_script = 'upload/upload-usecase.py'

prepare_main_script = 'prepare/start-prepare.py'

core_local_script = 'core/start-usecase-local.py'
core_deploy_script = 'core/start-usecase-deploy.py'
core_numbers_script = 'core/fill-numbers.py'
core_localhost_script = 'core/fill-localhost.py'

results_clear_script = 'results/clear-results.py'
results_retrieve_script = 'results/retrieve-results.py'
results_test_script = 'results/test-results.py'


# Paths
config_file_name = 'config.py'
tmp_config_file_name = 'tmp-config.py'
script_path = 'absfrontend-to-proactive/scripts'

tmp_gcmd_file_name = 'tmpGCMD'
tmp_gcma_file_name = 'tmpGCMA'
gcmd_file_name = '../usecase/resources/GCMD.xml'
gcma_file_name = '../usecase/resources/GCMA.xml'

tmp_application_file_name = 'tmpApp'
tmp_log_file_name = 'tmpLog'
application_file_name = '../usecase/src/abs/mapreduce/MapReduce.abs'
log_file_name = '../usecase/src/java/fr/inria/abstoproactive/logprocessing/LogProcessor.java'


# Deployment
local_option = '-local'
deploy_option = '-deploy'
host_file_name = 'prepare/host.names'
deployment_options = [local_option, deploy_option]

# Do not modify
nb_machines = 1
nb_more_deploy_per_machine = 1

