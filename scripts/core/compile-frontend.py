#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


working_dir = os.getcwd()

os.chdir("../absfrontend")
os.system("../apache-ant-1.9.3/bin/ant dist")
os.system("cp dist/absfrontend.jar ../usecase/lib/")

os.chdir(working_dir)


print config.logger_name, 'Ending', filename
	

  
