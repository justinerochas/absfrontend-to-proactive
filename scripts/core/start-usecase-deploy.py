#!/usr/bin/env python

import os
import config
import inspect

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


host_file = open(config.host_file_name, 'r')
launching_host = host_file.readline()[:-1]
host_file.close()

command = "ssh " + launching_host + " 'absfrontend-to-proactive/apache-ant-1.9.3/bin/ant -buildfile absfrontend-to-proactive/usecase/build.xml abs.run.java'"
os.system(command)

print config.logger_name, 'Ending', filename
