#!/usr/bin/env python

import os
import config
import inspect

# WARNING: works only locally

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


wd = os.getcwd()
	
os.chdir("../../../programming-multiactivities-and-priorities")
os.system("compile/build deploy.all")
	
os.system("cp dist/lib/ProActive.jar ../abs-deployment/absfrontend-to-proactive/absfrontend/lib")
os.system("cp dist/lib/ProActive.jar ../abs-deployment/absfrontend-to-proactive/usecase/lib/ProActive-lib")
		
os.chdir(wd)


print config.logger_name, 'Ending', filename
