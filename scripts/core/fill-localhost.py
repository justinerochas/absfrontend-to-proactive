#!/usr/bin/env python

import os
import config
import inspect

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Insert 'localhost' machine names in GCMD
tmp_gcmd_file = open(config.tmp_gcmd_file_name, 'w')
gcmd_file = open(config.gcmd_file_name, 'r')

hostnames = 'localhost ' * ((int(config.nb_machines) - 1) * (config.nb_more_deploy_per_machine + 1) + 1)
hostnames = hostnames[:-1]

for line in gcmd_file:
	if 'sshGroup' in line:
		tmp_gcmd_file.write('<sshGroup id="sshLan" hostList="' + hostnames + '" />' + os.linesep)
	else:
		tmp_gcmd_file.write(line)
		
tmp_gcmd_file.close()
gcmd_file.close()
		
os.system("mv " + config.tmp_gcmd_file_name + " " + config.gcmd_file_name)


# Change lib path in GCMA file
working_dir = os.getcwd()
path_pieces = working_dir.split('/')
path_pieces.pop()
path = ''
for piece in path_pieces:
	path = path + piece + '/'
tmp_gcma_file = open(config.tmp_gcma_file_name, 'w')
gcma_file = open(config.gcma_file_name, 'r')
	
for line in gcma_file:
	if '<proactive base="root" relpath=' in line:
		tmp_gcma_file.write('<proactive base="root" relpath="' + path + 'usecase/lib/absfrontend.jar:' + path + 'usecase/lib/ProActive-lib/*:">' + os.linesep)
	else:
		tmp_gcma_file.write(line)

tmp_gcma_file.close()
gcma_file.close()

os.system("mv " + config.tmp_gcma_file_name + " " + config.gcma_file_name)


print config.logger_name, 'Ending', filename
