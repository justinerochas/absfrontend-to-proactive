#!/usr/bin/env python

import os
import inspect
import config

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename

# Insert number of machines in GCMA file
tmp_gcma_file = open(config.tmp_gcma_file_name, 'w')
gcma_file = open(config.gcma_file_name, 'r')
	
for line in gcma_file:
	if '<nodeProvider refid' in line:
		tmp_gcma_file.write('<nodeProvider refid="WORKER_POOL" capacity="' + str((int(config.nb_machines) - 1) * (config.nb_more_deploy_per_machine + 1) + 1) + '" />' + os.linesep)
	else:
		tmp_gcma_file.write(line)

tmp_gcma_file.close()
gcma_file.close()

os.system("mv " + config.tmp_gcma_file_name + " " + config.gcma_file_name)


# Insert number of machines in application file (-1 because of the main machine)
tmp_application_file = open(config.tmp_application_file_name, 'w')
application_file = open(config.application_file_name, 'r')
	
for line in application_file:
	if 'def Int numNodes() =' in line:
		tmp_application_file.write('def Int numNodes() =' + str((int(config.nb_machines) - 1) * (config.nb_more_deploy_per_machine + 1)) + ';' + os.linesep)
	else:
		tmp_application_file.write(line)
		
tmp_application_file.close()
application_file.close()
		
os.system("mv " + config.tmp_application_file_name + " " + config.application_file_name)


# Insert number of machines in log checking file
tmp_log_file = open(config.tmp_log_file_name, 'w')
log_file = open(config.log_file_name, 'r')
	
for line in log_file:
	if 'private static final int nbNodes =' in line:
		tmp_log_file.write('private static final int nbNodes = ' + str((int(config.nb_machines) - 1) * (config.nb_more_deploy_per_machine + 1)) + ';' + os.linesep)
	else:
		tmp_log_file.write(line)
		
tmp_log_file.close()
log_file.close()
		
os.system("mv " + config.tmp_log_file_name + " " + config.log_file_name)


print config.logger_name, 'Ending', filename
