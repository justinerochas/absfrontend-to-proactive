#!/usr/bin/env python

import os
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


print config.logger_name, "Uploading usecase on", config.main_site, "site..."
# Delete existing usecase on Grid5000
os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + config.main_site + "/absfrontend-to-proactive/usecase/resources'")
os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + config.main_site + "/absfrontend-to-proactive/usecase/src'")
os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + config.main_site + "/absfrontend-to-proactive/scripts/config.py'")
# Send new version of usecase on Grid5000
os.system("scp -r ../usecase/resources " + config.login + "@" + config.access_site + ":" + config.main_site + "/absfrontend-to-proactive/usecase/resources")
os.system("scp -r ../usecase/src " + config.login + "@" + config.access_site + ":" + config.main_site + "/absfrontend-to-proactive/usecase/src")
os.system("scp ../scripts/config.py " + config.login + "@" + config.access_site + ":" + config.main_site + "/absfrontend-to-proactive/scripts/config.py")
print config.logger_name, "Upload usecase on", config.main_site, "done."


print config.logger_name, 'Ending', filename
