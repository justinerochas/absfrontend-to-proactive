#!/usr/bin/env python

import os
import inspect
import config

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Change lib path in GCMA file
tmp_gcma_file = open(config.tmp_gcma_file_name, 'w')
gcma_file = open(config.gcma_file_name, 'r')
	
for line in gcma_file:
	if '<proactive base="root" relpath=' in line:
		tmp_gcma_file.write('<proactive base="root" relpath="/home/jrochas/absfrontend-to-proactive/usecase/lib/absfrontend.jar:/home/jrochas/absfrontend-to-proactive/usecase/lib/ProActive-lib/*:">' + os.linesep)
	else:
		tmp_gcma_file.write(line)

tmp_gcma_file.close()
gcma_file.close()

os.system("mv " + config.tmp_gcma_file_name + " " + config.gcma_file_name)


print config.logger_name, 'Ending', filename
