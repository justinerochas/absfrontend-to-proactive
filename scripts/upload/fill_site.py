#!/usr/bin/env python

import os
import sys
import inspect
import config

filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Change site in config file
tmp_config_file = open(config.tmp_config_file_name, 'w')
config_file = open(config.config_file_name, 'r')
	
for line in config_file:
	if 'main_site' in line:
		tmp_config_file.write('main_site = \'' + sys.argv[5] + '\'' + os.linesep)
	else:
		tmp_config_file.write(line)

tmp_config_file.close()
config_file.close()

os.system("mv " + config.tmp_config_file_name + " " + config.config_file_name)

# Change site in memory also 
config.main_site = sys.argv[5]


print config.logger_name, 'Ending', filename
