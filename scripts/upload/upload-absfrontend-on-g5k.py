#!/usr/bin/env python

import os
import sys
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


wd = os.getcwd()
	
os.chdir("../..")

for site in config.sites:
	print config.logger_name, "Uploading on", site, "site..."
	# Delete existing absfrontend on Grid5000
	os.system("ssh " + config.login + "@" + config.access_site + " 'rm -Rf " + site + "/absfrontend-to-proactive'")
	# Send new version of absfrontend on Grid5000
	os.system("scp -r absfrontend-to-proactive " + config.login + "@" + config.access_site + ":" + site + "/")
	print config.logger_name, "Upload on", site, "done."
	
os.chdir(wd)


print config.logger_name, 'Ending', filename
