#!/usr/bin/env python

import sys
import config
import inspect


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Running', filename


# Parameters checking
if (not len(sys.argv) == 4 and not len(sys.argv) == 7) or not sys.argv[1] in config.compilation_options or not sys.argv[2] in config.deployment_options or not sys.argv[3].isdigit() :
	print config.logger_name, 'Usage: python run.py + ' + str(config.compilation_options) + ' + ' + str(config.deployment_options) + ' + \'nb-machines\' + [\'hh:mm:ss\' \'site-name\' \'cluster-name\']' 
	exit()


# Scaling
config.nb_machines = sys.argv[3]
if sys.argv[2] == config.local_option:
	execfile(config.core_localhost_script)
execfile(config.core_numbers_script)
	
	
# Compilation
if sys.argv[1] == config.compilation_options[1] or sys.argv[1] == config.compilation_options[3]:
	execfile(config.compile_proactive_script)
if sys.argv[1] == config.compilation_options[2] or sys.argv[1] == config.compilation_options[3]:
	execfile(config.compile_frontend_script)


# Deployment
if sys.argv[2] == config.deploy_option:
	execfile(config.upload_fill_site_script)
	if not sys.argv[1] == config.compilation_options[0]:
		execfile(config.upload_main_script)
	else:
		execfile(config.upload_usecase_script)
	command = 'ssh ' + config.login + '@' + config.access_site + ' \'python  ' + config.main_site + '/' + config.script_path + '/' + 'prepare.py' + ' ' + sys.argv[4] + ' ' + sys.argv[6] + ' ' + sys.argv[3] + '\'' 
	os.system(command)
	

# Executing	
if sys.argv[2] == config.local_option:
	execfile(config.core_local_script) 
if sys.argv[2] == config.deploy_option:
	execfile(config.results_clear_script)
	command = 'ssh ' + config.login + '@' + config.access_site + ' \'python  ' + config.main_site + '/' + config.script_path + '/' + 'usecase.py' + '\'' 
	os.system(command)


# Results
if sys.argv[2] == config.deploy_option:
	execfile(config.results_retrieve_script)
execfile(config.results_test_script)


filename = inspect.getfile(inspect.currentframe())
print config.logger_name, 'Ending', filename

